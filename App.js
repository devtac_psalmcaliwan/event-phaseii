//Authentocation
import LoginIndex from "./src/Authentication/Login/index";
import RegistrationIndex from "./src/Authentication/Registration/index";

//Events
import EventsIndex from "./src/Events/index";
import Open from "./src/Events/components/Open";
//Contacts
import ContactsIndex from "./src/Contacts/index";
import Platform from "./src/Contacts/platform";
import mycontacts from "./src/Contacts/myContacts";
import OpenContact from "./src/Contacts/components/OpenContact";
import ContactSearch from "./src/Contacts/ContactSearch";

//Menu
import MenuIndex from "./src/Menu/index";
import MyFeeds from "./src/Menu/components/MyFeeds";
import ViewEvent from "./src/Menu/ViewEvents";
import EventMembers from "./src/Menu/components/EventMembers";
import EventUpdate from "./src/Menu/components/EventUpdate";
//Events Creation
import eventCreactionPhaseI from "./src/Menu/CreateEvents/phaseI";
import eventCreactionPhaseII from "./src/Menu/CreateEvents/phaseII";

//Notification
import NotificationIndex from "./src/Notification/index";
import contactNotif from "./src/Notification/contactNotif";

//Navigation
import NavigationIndex from "./src/NavigationBar/index";

//Maps
import RootMap from "./src/Maps/RootMap";

//Profile
import ProfileIndex from "./src/Menu/Profile/Index";
import ChangePassword from "./src/Menu/Profile/changePassword";

//RSVP
import RSVPContacts from "./src/Menu/components/RSVPContacts";
import openEvent from "./src/Notification/openEvent";

//Group
import GroupIndex from "./src/Menu/Groups/Index";
import GroupCreator from "./src/Menu/Groups/GroupCreator";
import GroupOpen from "./src/Menu/Groups/Open";
import Settings from "./src/Menu/Groups/Settings";
import GroupMembers from "./src/Menu/Groups/Members";
import groupInvite from "./src/Menu/Groups/components/groupInvite";

import React, { Component } from "react";
import { createStackNavigator } from "react-navigation";
import MapView, { Marker } from "react-native-maps";

import { Root, Container, Content, Header } from "native-base";

import { LocationTracker } from "./src/Library/LocationTracker";
import { from } from "rxjs";
import { StatusBar, View } from "react-native";
export default class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Root>
        <Routes />
      </Root>
    );
  }
}

const Routes = createStackNavigator(
  {
    //Authentication
    LoginIndex: LoginIndex,
    RegistrationIndex: RegistrationIndex,

    //Notification
    NotificationIndex: NotificationIndex,
    contactNotif: contactNotif,

    //Menu
    MenuIndex: MenuIndex,
    MyFeeds: MyFeeds,
    ViewEvent: ViewEvent,
    EventMembers: EventMembers,
    EventUpdate: EventUpdate,

    //Events Creation
    eventCreactionPhaseI: eventCreactionPhaseI,
    eventCreactionPhaseII: eventCreactionPhaseII,

    //Contact
    ContactsIndex: ContactsIndex,
    Platform: Platform,
    mycontacts: mycontacts,
    OpenContact: OpenContact,
    ContactSearch: ContactSearch,

    //Events
    EventsIndex: EventsIndex,
    Open: Open,

    //Navigation
    NavigationIndex: NavigationIndex,

    //Map
    RootMap: RootMap,

    //Profile
    ProfileIndex: ProfileIndex,
    ChangePassword: ChangePassword,

    //RSVP
    RSVPContacts: RSVPContacts,
    openEvent: openEvent,

    //Group
    GroupIndex: GroupIndex,
    GroupCreator: GroupCreator,
    GroupOpen: GroupOpen,
    Settings: Settings,
    GroupMembers: GroupMembers,
    groupInvite: groupInvite
  },
  {
    initialRouteName: "LoginIndex",
    headerMode: "none"
  }
);
console.disableYellowBox = true;
