import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Left,
  Right,
  Body
} from "native-base";
import { Auth } from "./../../Library/Auth";
import { Uploader } from "./../../Library/uploader";
import Spinner from "react-native-loading-spinner-overlay";
import firebase from "react-native-firebase";
import { View, Image, Alert } from "react-native";
import ImagePicker from "react-native-image-crop-picker";
export default class RegistrationIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      first: "",
      middle: "",
      last: "",
      mobile: "",
      visible: false,
      spinnerMessage: "",
      image: {},
      imgURI:
        "https://firebasestorage.googleapis.com/v0/b/event-management-66d93.appspot.com/o/assets%2Fif_user_1287507.png?alt=media&token=dcb2386a-69bd-43cb-805f-732776392745"
    };
  }

  componentDidMount() {
    let params = this.props.navigation.getParam("params");

    if (params != null) {
      const sendTo = params.sendTo == null ? params.mobile : params.sendTo;

      if (sendTo.includes("@")) {
        this.setState({
          email: sendTo
        });
      } else {
        this.setState({
          mobile: sendTo
        });
      }
    } else {
    }
  }

  openPicker() {
    ImagePicker.openPicker({
      width: 300,
      height: 400
    }).then(image => {
      this.setState({
        image: image,
        imgURI: image.path
      });
    });
  }

  openCamera() {
    ImagePicker.openCamera({
      width: 300,
      height: 400
    }).then(image => {
      this.setState({
        image: image,
        imgURI: image.path
      });
    });
  }

  upload() {
    Uploader.UploadUserImage(this.state.image).then(url => {
      let imgData = url.downloadURL;
      this.register(imgData);
    });
  }

  register(imgData) {
    this.setState({
      spinnerMessage: "Checking Database if Credentials are available"
    });
    let creds = {
      email: this.state.email,
      password: this.state.password
    };
    try {
      Auth.createUserEmailPassword(creds).then(data => {
        let id = data.user._user.uid;
        this.fillUserInfo(id, imgData);
      });
    } catch (error) {
      this.setState({
        spinnerMessage: error,
        visible: false
      });
    }
  }

  fillUserInfo(id, imgData) {
    let data = {
      name: {
        first: this.state.first,
        middle: this.state.middle,
        last: this.state.last
      },
      mobile: this.state.mobile,
      email: this.state.email,
      password: this.state.password,
      userType: "user",
      flag: "Active",
      dateRegistered: Date(),
      userID: id,
      imageURL: imgData
    };

    firebase
      .firestore()
      .collection("users")
      .doc(id)
      .set(data)
      .then(() => {
        this.setState({
          spinnerMessage: "Registration Successful",
          visible: false
        });
        this.props.navigation.navigate("LoginIndex", { trigger: true });
      });
  }

  render() {
    return (
      <Container>
        <Header />
        <Content style={{ paddingTop: 40 }}>
          <Spinner
            visible={this.state.visible}
            textContent={"Creating your account"}
            textStyle={{ color: "#FFF" }}
          />
          <View>
            <Left />
            <Body>
              <Image
                source={{
                  uri: this.state.imgURI
                }}
                style={{ width: 128, height: 128, borderRadius: 128 / 2 }}
              />
            </Body>
            <Right />
          </View>

          <View>
            <Left />
            <Body>
              <Button
                transparent
                success
                onPress={() => {
                  Alert.alert(
                    "Where do you want your photo from",
                    "",
                    [
                      {
                        text: "CAMERA",
                        onPress: () => {
                          this.openCamera();
                        }
                      },
                      {
                        text: "GALLERY",
                        onPress: () => {
                          this.openPicker();
                        }
                      }
                    ],
                    { cancelable: true }
                  );
                }}
              >
                <Text>Change Profile Picture</Text>
              </Button>
            </Body>
            <Right />
          </View>

          <Form>
            <Item inlineLabel>
              <Label>Email</Label>
              <Input
                value={this.state.email}
                onChangeText={text => this.setState({ email: text })}
              />
            </Item>

            <Item inlineLabel>
              <Label>Mobile Number</Label>
              <Input
                value={this.state.mobile}
                onChangeText={text => this.setState({ mobile: text })}
              />
            </Item>
            <Item inlineLabel>
              <Label>Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={text => this.setState({ password: text })}
              />
            </Item>

            <Item inlineLabel>
              <Label>First Name</Label>
              <Input
                value={this.state.first}
                onChangeText={text =>
                  this.setState({
                    first: text
                  })
                }
              />
            </Item>

            <Item inlineLabel>
              <Label>Middle Name</Label>
              <Input
                value={this.state.middle}
                onChangeText={text => this.setState({ middle: text })}
              />
            </Item>

            <Item inlineLabel>
              <Label>Last Name</Label>
              <Input
                value={this.state.last}
                onChangeText={text =>
                  this.setState({
                    last: text
                  })
                }
              />
            </Item>
          </Form>

          <Content style={{ padding: 50 }}>
            <Button
              full
              success
              onPress={() => {
                this.setState({ visible: true }), this.upload();
              }}
            >
              <Text>Register</Text>
            </Button>
          </Content>
        </Content>
      </Container>
    );
  }
}
