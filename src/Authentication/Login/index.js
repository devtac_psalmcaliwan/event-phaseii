import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text
} from "native-base";
import firebase from "react-native-firebase";
//Library
import { Auth } from "./../../Library/Auth";
import { Linking, Alert } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
export default class LoginIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      linker: false,
      linkData: "",
      trigger: this.props.navigation.getParam("trigger"),
      params: "",
      visible: false,
      alertmessage: "",
      alerttrigger: false
    };

    this.checkDeviceContact = this.checkDeviceContact(this);
  }

  componentDidMount() {
    Linking.getInitialURL()
      .then(url => {
        if (url) {
          const initLink = url;
          const link = initLink.slice(-20);

          this.checkLink(link);
        }
      })
      .catch(err => console.error("An error occurred", err));
  }

  checkLink(initLink) {
    firebase
      .firestore()
      .collection("deviceContacts")
      .doc(initLink)
      .get()
      .then(res => {
        let param = res.data();
        this.props.navigation.navigate("RegistrationIndex", {
          params: param
        });
      });
  }

  checkDeviceContact(initLink) {
    let param = "";

    return param;
  }

  loginPress() {
    let creds = {
      email: this.state.email,
      password: this.state.password
    };

    Auth.emailPassword(creds)
      .then(() => {
        this.setState({
          visible: false
        });
        this.props.navigation.navigate("EventsIndex");
      })
      .catch(err => {
        this.setState({
          visible: false
        });
        if (err.code == "auth/user-not-found") {
          let msg = "There is no user with a email " + this.state.email;
          Alert.alert(msg, "", [{ text: "OK" }], { cancelable: false });
        } else if (err.code == "auth/wrong-password") {
          let msg = "Wrong password please try again";
          Alert.alert(msg, "", [{ text: "OK" }], { cancelable: false });
        }
      });
  }

  registerpress() {
    this.props.navigation.navigate("RegistrationIndex");
  }
  render() {
    if (this.state.linker && this.state.trigger == false) {
      this.props.navigation.navigate("RegistrationIndex", {
        params: this.state.linkData
      });
    }
    return (
      <Container>
        <Header />
        <Content>
          <Spinner
            visible={this.state.visible}
            textContent={"Logging In... Please wait"}
            textStyle={{ color: "#FFF" }}
          />
          <Form>
            {this.state.alerttrigger ? (
              <Button
                full
                danger
                onPress={() => {
                  this.setState({ alerttrigger: false });
                }}
              >
                {this.state.alertmessage}
              </Button>
            ) : null}
            <Item floatingLabel>
              <Label>Email</Label>
              <Input
                value={this.state.email}
                onChangeText={text => this.setState({ email: text })}
              />
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={text => this.setState({ password: text })}
              />
            </Item>
          </Form>

          <Content style={{ padding: 20 }}>
            <Button
              success
              full
              onPress={() => {
                this.setState({ visible: true }), this.loginPress();
              }}
            >
              <Text>LOGIN</Text>
            </Button>

            <Button
              danger
              transparent
              full
              onPress={() => {
                this.registerpress();
              }}
            >
              <Text>REGISTER</Text>
            </Button>
          </Content>
        </Content>
      </Container>
    );
  }
}
