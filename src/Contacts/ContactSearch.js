import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Item,
  Input,
  Separator,
  Thumbnail
} from "native-base";

import firebase from "react-native-firebase";

import NavigationIndex from "./../NavigationBar/index";
import { Auth } from "./../Library/Auth";
import { TouchableOpacity, View } from "react-native";
export default class ContactSearch extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      search: "",
      ready: false,
      platform: [],
      myContact: []
    };
  }

  search() {
    firebase
      .firestore()
      .collection("users")
      .where("email", "==", this.state.search)
      .onSnapshot(doc => {
        let contact = new Array();

        doc.forEach(doc => {
          contact.push(doc.data());

          this.setState({
            platform: contact,
            ready: true
          });
        });
      });

    firebase
      .firestore()
      .collection("user-contacts")
      .where("email", "==", this.state.search)
      .where("owner", "==", this.userData.uid)
      .onSnapshot(doc => {
        let contact = new Array();

        doc.forEach(doc => {
          contact.push(doc.data());

          this.setState({
            myContact: contact,
            ready: true
          });
        });
      });
  }
  render() {
    return (
      <Container>
        <NavigationIndex contacts={"white"} />
        {/* <ContactNavigation search={"#5067FF"} area={"search"} /> */}

        <Header style={{ backgroundColor: "white" }} searchBar rounded>
          <Left>
            <Icon
              name="ios-search"
              style={{
                color: "#5067FF"
              }}
            />
          </Left>
          <Body>
            <Item style={{ width: 200 }}>
              <Input
                placeholder="Search here"
                value={this.state.search}
                onChangeText={text => this.setState({ search: text })}
              />
            </Item>
          </Body>
          <Right>
            <TouchableOpacity
              style={{ paddingLeft: 10 }}
              onPress={() => {
                this.search();
              }}
            >
              <Icon
                type="Entypo"
                name="chevron-with-circle-right"
                style={{
                  color: "#5067FF"
                }}
              />
            </TouchableOpacity>

            <TouchableOpacity
              style={{ paddingLeft: 10 }}
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialCommunityIcons"
                name="cancel"
                style={{
                  color: "#5067FF"
                }}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <List>
            <Separator>
              <Text>Device</Text>
            </Separator>

            <Separator>
              <Text>Platform</Text>
            </Separator>

            {this.state.platform.map(p => (
              <ListItem thumbnail>
                <Left>
                  <Thumbnail source={{ uri: p.imageURL }} />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("ProfileIndex", {
                        param: p.userID
                      });
                    }}
                  >
                    <Text>
                      {p.name.first} {p.name.middle} {p.name.last}
                    </Text>
                  </TouchableOpacity>
                </Body>
              </ListItem>
            ))}

            <Separator>
              <Text>My Contact</Text>
            </Separator>

            {this.state.myContact.map(mC => (
              <ListItem>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("ProfileIndex", {
                      param: mC.hisID
                    });
                  }}
                >
                  <Text>{mC.name}</Text>
                </TouchableOpacity>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}
