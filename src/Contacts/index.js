import React, { Component } from "react";
import Spinner from "react-native-loading-spinner-overlay";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Icon,
  Text,
  Fab,
  Button,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  List,
  ListItem
} from "native-base";
import Modal from "react-native-modal";
import Contacts from "react-native-contacts";
import { TouchableOpacity, View, PermissionsAndroid } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import ContactFeeds from "./components/feeds";
import { Generate } from "./../Library/generate";
import firebase from "react-native-firebase";
import { tryCatch } from "rxjs/internal/util/tryCatch";
import NavigationIndex from "./../NavigationBar/index";
import ContactNavigation from "./components/contactNavigation";
import { Auth } from "./../Library/Auth";

export default class ContactsIndex extends Component {
  arraySort = require("array-sort");
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      active: false,
      isModalVisible: false,
      smsTrigger: false,
      sent: false,
      contacts: [],
      ready: false,
      phoneTrigger: false,
      phoneNumber: [],
      email: [],
      smssent: false,
      visible: true,
      ready: false,

      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      }
    };
  }

  componentWillMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      })
      .then(() => {
        this.getContacts();
      });
  }

  getContacts() {
    Contacts.checkPermission((err, permission) => {
      console.log(permission);

      if (err) throw err;

      if (permission === "undefined") {
        Contacts.requestPermission((err, permission) => {});
      }
      if (permission === "authorized") {
        Contacts.getAllWithoutPhotos((err, contacts) => {
          if (err) {
            console.log(err);
          } else {
            let mycontacts = contacts;
            let results = this.arraySort(mycontacts, ["givenName"]);

            this.setState({
              contacts: results,
              visible: false,
              ready: true
            });
          }
        });
      }
      if (permission === "denied") {
        console.log(permission);
      }
    });
  }

  sendEmailLink(email, message) {
    const invitationID = Generate.generateID();
    const httpLink = "https://mobileeventmanager.page.link/invite/eventid=";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${invitationID}`;
    this._toggleModal();

    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    firebase
      .links()
      .createDynamicLink(link)
      .then(url => {
        const linkURL = url;
        let data = {
          linkURL,
          linkID,
          sendTo: email,
          message,
          invitationID
        };

        this.saveEmailLink(data);
      });
  }

  checkMobileLenght(mobile) {}

  convertToE164formart(mobile) {
    let sliced = mobile.slice(1);
    let e164 = "+63" + sliced;
    return e164;
  }

  sendSMSLink(mobile, message) {
    this.checkMobileLenght(mobile);
    let sendTo = this.convertToE164formart(mobile);

    const invitationID = Generate.generateID();
    const httpLink = "https://mobileeventmanager.page.link/invite/";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${invitationID}`;
    this._toggleSMS();

    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        let data = {
          linkURL,
          linkID,
          sendTo,
          message,
          invitationID
        };

        this.saveLink(data);
      });
  }

  sendSMSLink(mobile, message) {
    this.checkMobileLenght(mobile);
    let sendTo = this.convertToE164formart(mobile);

    const invitationID = Generate.generateID();
    const httpLink = "https://mobileeventmanager.page.link/invite/";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${invitationID}`;
    this._toggleSMS();

    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        let data = {
          linkURL,
          linkID,
          sendTo,
          message,
          invitationID
        };

        this.mobilesaveLink(data);
      });
  }

  saveEmailLink(placeholder) {
    let data = {
      invitationID: placeholder.invitationID,
      linkURL: placeholder.linkURL,
      linkID: placeholder.linkID,
      sendTo: placeholder.sendTo,
      message: placeholder.message,
      status: "unused",
      dateSent: Date.now()
    };

    firebase
      .firestore()
      .collection("EmailLinks")
      .doc(data.invitationID)
      .set(data)
      .then(() => {
        this.setState({
          sent: true
        });
      });
  }

  mobilesaveLink(placeholder) {
    let data = {
      invitationID: placeholder.invitationID,
      linkURL: placeholder.linkURL,
      linkID: placeholder.linkID,
      sendTo: placeholder.sendTo,
      message: placeholder.message,
      status: "unused",
      dateSent: Date.now()
    };

    firebase
      .firestore()
      .collection("MobileLinks")
      .doc(data.invitationID)
      .set(data)
      .then(() => {
        this.setState({
          sent: true
        });
      });
  }

  contactNotification(param) {
    contactNotif = {
      notificationID: Generate.generateID(),
      senderID: param.senderID,
      senderName: param.sender,
      sendTo: param.mobile
    };

    firebase
      .firestore()
      .collection("ContactNotification")
      .doc(contactNotif.notificationID)
      .set(contactNotif);
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  _toggleSMS = () => this.setState({ smsTrigger: !this.state.smsTrigger });

  _phoneTrigger = () =>
    this.setState({ phoneTrigger: !this.state.phoneTrigger });

  showSentMessage() {
    if (this.state.sent == true) {
      return (
        <Content>
          <Button
            full
            success
            onPress={() => {
              this.setState({ sent: false });
            }}
          >
            <Text>Message Sent</Text>
          </Button>
        </Content>
      );
    } else {
      return null;
    }
  }

  createNewDeviceContact(param) {
    const id = Generate.generateID();

    const httpLink = "https://mobileeventmanager.page.link/invite/eventid=";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${id}`;
    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        const space = " ";
        let data = {
          docID: id,
          senderID: this.state.user.userID,
          mobile: param.number,
          linkURL: linkURL,
          sender: {
            name:
              this.state.user.name.first +
              space +
              this.state.user.name.middle +
              space +
              this.state.user.name.last,
            email: this.state.user.email,
            mobile: this.state.user.mobile
          }
        };

        firebase
          .firestore()
          .collection("deviceContacts")
          .doc(id)
          .set(data)
          .then(() => {
            this.contactNotification(data);
            this.setState({ smssent: true });
          });
      });
  }

  getData(val) {}

  showPhoneModal() {
    return (
      <View style={{ flex: 1 }}>
        <Modal isVisible={this.state.phoneTrigger}>
          {this.state.smssent ? (
            <View>
              <Button
                success
                full
                onPress={() => {
                  this.setState({ smssent: false });
                }}
              >
                <Text>SMS Invitation Sent</Text>
              </Button>
            </View>
          ) : null}
          <Card>
            {this.state.phoneNumber.map(pN => (
              <CardItem>
                <Grid>
                  <Col>
                    <Text>{pN.label}</Text>
                  </Col>
                  <Col>
                    <TouchableOpacity
                      onPress={() => {
                        this.createNewDeviceContact(pN);
                      }}
                    >
                      <Text>{pN.number}</Text>
                    </TouchableOpacity>
                  </Col>
                </Grid>
              </CardItem>
            ))}

            <CardItem>
              <Button
                danger
                transparent
                onPress={() => {
                  this._phoneTrigger();
                }}
              >
                <Text>Close</Text>
              </Button>
            </CardItem>
          </Card>
        </Modal>
      </View>
    );
  }

  render() {
    return (
      <Container>
        <NavigationIndex contacts={"white"} />
        <ContactNavigation index={"#5067FF"} area={"device"} />
        <Content>
          <View style={{ flex: 1 }}>
            <Modal isVisible={this.state.isModalVisible}>
              <Card>
                <CardItem>
                  <Item>
                    <Input
                      placeholder="Email"
                      value={this.state.linkEmail}
                      onChangeText={text => this.setState({ linkEmail: text })}
                    />
                  </Item>
                </CardItem>

                <CardItem>
                  <Item>
                    <Input
                      placeholder="Message"
                      value={this.state.linkMessage}
                      onChangeText={text =>
                        this.setState({ linkMessage: text })
                      }
                    />
                  </Item>
                </CardItem>
                <CardItem>
                  <Body>
                    <Button
                      transparent
                      success
                      onPress={() => {
                        this.sendEmailLink(
                          this.state.linkEmail,
                          this.state.linkMessage
                        );
                      }}
                    >
                      <Text>Send</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Button transparent danger onPress={this._toggleModal}>
                      <Text>Cancel</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
            </Modal>
          </View>

          <View style={{ flex: 1 }}>
            <Modal isVisible={this.state.smsTrigger}>
              <Card>
                <CardItem>
                  <Item>
                    <Input
                      placeholder="Mobile Number"
                      value={this.state.linkMobile}
                      onChangeText={text => this.setState({ linkMobile: text })}
                    />
                  </Item>
                </CardItem>

                <CardItem>
                  <Item>
                    <Input
                      placeholder="Message"
                      value={this.state.linkMessage}
                      onChangeText={text =>
                        this.setState({ linkMessage: text })
                      }
                    />
                  </Item>
                </CardItem>
                <CardItem>
                  <Body>
                    <Button
                      transparent
                      success
                      onPress={() => {
                        this.sendSMSLink(
                          this.state.linkMobile,
                          this.state.linkMessage
                        );
                      }}
                    >
                      <Text>Send</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Button transparent danger onPress={this._toggleSMS}>
                      <Text>Cancel</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
            </Modal>
          </View>
          {this.showSentMessage()}
          {this.state.ready ? null : (
            <View>
              <Text>Importing Contacts. Please Wait</Text>
            </View>
          )}
          <List>
            {this.state.contacts.map(c => (
              <ListItem>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("OpenContact", {
                      params: c
                    });
                  }}
                >
                  <Text>
                    {c.givenName} {c.middleName} {c.familyName}
                  </Text>
                </TouchableOpacity>
              </ListItem>
            ))}
          </List>

          {this.showPhoneModal()}
        </Content>

        <Fab
          active={this.state.active}
          direction="up"
          style={{ backgroundColor: "#5067FF" }}
          position="bottomRight"
          onPress={() => this.setState({ active: !this.state.active })}
        >
          <Icon type="MaterialIcons" name="person-add" />

          <Button
            style={{ backgroundColor: "#34A34F" }}
            onPress={() => {
              this.setState({
                smsTrigger: true
              });
            }}
          >
            <Icon name="logo-whatsapp" />
          </Button>

          <Button
            style={{ backgroundColor: "#DD5144" }}
            onPress={this._toggleModal}
          >
            <Icon name="mail" />
          </Button>
        </Fab>
      </Container>
    );
  }
}
