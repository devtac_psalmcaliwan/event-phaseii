import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Icon
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { withNavigation } from "react-navigation";
import firebase from "react-native-firebase";

class ContactNavigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTrigger: false,
      search: "",
      platform: []
    };
  }

  render() {
    return (
      <Header style={{ backgroundColor: "white" }} searchBar rounded>
        <Grid>
          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("ContactsIndex");
              }}
            >
              <Icon
                name={"camera-front"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.index
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Platform");
              }}
            >
              <Icon
                name={"group-work"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.platform
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("mycontacts");
              }}
            >
              <Icon
                name={"group"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.mycontacts
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("ContactSearch");
              }}
            >
              <Icon
                name="ios-search"
                style={{
                  color: this.props.search
                }}
              />
            </TouchableOpacity>
          </Col>
        </Grid>
      </Header>
    );
  }
}
export default withNavigation(ContactNavigation);
