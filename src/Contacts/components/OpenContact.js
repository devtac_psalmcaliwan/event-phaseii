import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  List,
  ListItem,
  Button,
  Switch,
  Separator
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet, Alert } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import firebase from "react-native-firebase";
import { Auth } from "./../../Library/Auth";
import { Generate } from "./../../Library/generate";
export default class OpenContact extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      }
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      });
  }

  createNewDeviceContact(param) {
    const id = Generate.generateID();

    const httpLink = "https://mobileeventmanager.page.link/invite/eventid=";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${id}`;
    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        const space = " ";
        let data = {
          docID: id,
          senderID: this.state.user.userID,
          mobile: param.number,
          email: param.email,
          linkURL: linkURL,
          sender: {
            name:
              this.state.user.name.first +
              space +
              this.state.user.name.middle +
              space +
              this.state.user.name.last,
            email: this.state.user.email,
            mobile: this.state.user.mobile
          }
        };

        firebase
          .firestore()
          .collection("deviceContacts")
          .doc(id)
          .set(data)
          .then(() => {
            this.contactNotification(data);
            this.setState({ smssent: true });
          });
      });
  }

  contactNotification(param) {
    contactNotif = {
      notificationID: Generate.generateID(),
      senderID: param.senderID,
      senderName: param.sender,
      mobile: param.mobile,
      email: param.email
    };

    firebase
      .firestore()
      .collection("ContactNotification")
      .doc(contactNotif.notificationID)
      .set(contactNotif);
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Contact Details</Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"cancel"} />
            </Button>
          </Right>
        </Header>
        <Content>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              padding: 10,
              backgroundColor: "gray"
            }}
          >
            <Image
              source={{ uri: this.state.params.thumbnailPath }}
              style={{
                height: 150,
                width: 150,
                borderRadius: 150
              }}
            />
          </View>

          <List>
            <Separator style={{ backgroundColor: "#1E1E1E" }} />

            <Separator>
              <Text>Personal Information</Text>
            </Separator>

            <ListItem>
              <Grid>
                <Row>
                  <Col style={{ width: "20%" }}>
                    <Text note>Name:</Text>
                  </Col>
                  <Col>
                    <Text>
                      {this.state.params.givenName}{" "}
                      {this.state.params.middleName}{" "}
                      {this.state.params.familyName}
                    </Text>
                  </Col>
                </Row>
              </Grid>
            </ListItem>

            <Separator>
              <Text>Phone Numbers</Text>
            </Separator>

            {this.state.params.phoneNumbers.map(pN => (
              <ListItem>
                <Grid>
                  <Row>
                    <Col style={{ width: "20%" }}>
                      <Text note style={{ textAlign: "left" }}>
                        {pN.label}
                      </Text>
                    </Col>
                    <Col style={{ width: "70%" }}>
                      <Text style={{ textAlign: "left" }}>{pN.number}</Text>
                    </Col>

                    <Col>
                      <TouchableOpacity
                        onPress={() => {
                          this.createNewDeviceContact(
                            (param = { number: pN.number })
                          );
                        }}
                      >
                        <Icon
                          type={"Entypo"}
                          name={"message"}
                          style={{ color: "#5CB85C" }}
                        />
                      </TouchableOpacity>
                    </Col>
                  </Row>
                </Grid>
              </ListItem>
            ))}

            <Separator>
              <Text>Email Addresses</Text>
            </Separator>
            {this.state.params.emailAddresses.map(eA => (
              <ListItem>
                <Grid>
                  <Row>
                    <Col style={{ width: "20%" }}>
                      <Text note style={{ textAlign: "left" }}>
                        {eA.label}
                      </Text>
                    </Col>
                    <Col style={{ width: "70%" }}>
                      <Text style={{ textAlign: "left" }}>{eA.email}</Text>
                    </Col>

                    <Col>
                      <TouchableOpacity
                        onPress={() => {
                          this.createNewDeviceContact(
                            (param = { email: eA.email })
                          );
                        }}
                      >
                        <Icon
                          type={"Ionicons"}
                          name={"md-mail"}
                          style={{ color: "#D9534F" }}
                        />
                      </TouchableOpacity>
                    </Col>
                  </Row>
                </Grid>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}
