import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Fab,
  Button,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  List,
  ListItem,
  Thumbnail,
  Icon
} from "native-base";
import Modal from "react-native-modal";
import Contacts from "react-native-contacts";
import { TouchableOpacity, View } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import ContactFeeds from "./components/feeds";
import { Generate } from "./../Library/generate";
import firebase from "react-native-firebase";
import { tryCatch } from "rxjs/internal/util/tryCatch";
import NavigationIndex from "./../NavigationBar/index";
import ContactNavigation from "./components/contactNavigation";
export default class Platform extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: false,
      isModalVisible: false,
      smsTrigger: false,
      sent: false,
      contacts: [],
      ready: false,
      phoneTrigger: false,
      phoneNumber: [],
      platform: []
    };
  }

  componentWillMount() {
    firebase
      .firestore()
      .collection("users")

      .onSnapshot(doc => {
        let contact = new Array();

        doc.forEach(doc => {
          contact.push(doc.data());

          this.setState({
            platform: contact,
            ready: true
          });
        });
      });
  }

  getData() {}

  render() {
    if (this.state.ready == true) {
      return (
        <Container>
          <NavigationIndex contacts={"white"} />
          <ContactNavigation platform={"#5067FF"} area={"platform"} />

          <List>
            {this.state.platform.map(p => (
              <ListItem avatar>
                <Left>
                  <Thumbnail source={{ uri: p.imageURL }} />
                </Left>
                <Body>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("ProfileIndex", {
                        param: p.userID
                      });
                    }}
                  >
                    <Text>
                      {p.name.first} {p.name.middle} {p.name.last}
                    </Text>
                  </TouchableOpacity>
                </Body>
                <Right>
                  {/* <Icon type="Ionicons" name="md-person-add" /> */}
                </Right>
              </ListItem>
            ))}
          </List>

          <Fab
            active={this.state.active}
            direction="up"
            style={{ backgroundColor: "#5067FF" }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}
          >
            <Icon type="MaterialIcons" name="person-add" />

            <Button
              style={{ backgroundColor: "#34A34F" }}
              onPress={this._toggleSMS}
            >
              <Icon name="logo-whatsapp" />
            </Button>

            <Button
              style={{ backgroundColor: "#DD5144" }}
              onPress={this._toggleModal}
            >
              <Icon name="mail" />
            </Button>
          </Fab>
        </Container>
      );
    } else {
      return null;
    }
  }
}
