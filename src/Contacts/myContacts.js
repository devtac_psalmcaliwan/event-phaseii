import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Icon,
  Text,
  Fab,
  Button,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  List,
  ListItem
} from "native-base";
import Modal from "react-native-modal";
import Contacts from "react-native-contacts";
import { TouchableOpacity, View } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import ContactFeeds from "./components/feeds";
import { Generate } from "./../Library/generate";
import firebase from "react-native-firebase";
import { tryCatch } from "rxjs/internal/util/tryCatch";
import NavigationIndex from "./../NavigationBar/index";
import ContactNavigation from "./components/contactNavigation";
import { Auth } from "./../Library/Auth";
export default class mycontacts extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      active: false,
      isModalVisible: false,
      smsTrigger: false,
      sent: false,
      contacts: [],
      ready: false,
      phoneTrigger: false,
      phoneNumber: [],
      area: "",
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      },
      loader: false,
      contacts: []
    };

    this.getResults = this.getResults.bind(this);
  }
  componentWillMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data(),
          ready: true
        });
      });
  }

  getResults() {
    firebase
      .firestore()
      .collection("user-contacts")
      .where("owner", "==", this.state.user.userID)
      .onSnapshot(doc => {
        let contact = new Array();

        doc.forEach(doc => {
          contact.push(doc.data());

          this.setState({
            contacts: contact,
            ready: true
          });
        });
      });
  }

  getData() {}
  render() {
    if (this.state.ready == true) {
      return (
        <Container>
          {this.getResults()}
          <NavigationIndex contacts={"white"} />
          <ContactNavigation mycontacts={"#5067FF"} area={"myContacts"} />
          <Content>
            <List>
              {this.state.contacts.map(c => (
                <ListItem>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate("ProfileIndex", {
                        param: c.hisID
                      });
                    }}
                  >
                    <Text>{c.name} </Text>
                  </TouchableOpacity>
                </ListItem>
              ))}
            </List>
          </Content>

          <Fab
            active={this.state.active}
            direction="up"
            style={{ backgroundColor: "#5067FF" }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}
          >
            <Icon type="MaterialIcons" name="person-add" />

            <Button
              style={{ backgroundColor: "#34A34F" }}
              onPress={this._toggleSMS}
            >
              <Icon name="logo-whatsapp" />
            </Button>

            <Button
              style={{ backgroundColor: "#DD5144" }}
              onPress={this._toggleModal}
            >
              <Icon name="mail" />
            </Button>
          </Fab>
        </Container>
      );
    } else {
      return (
        <Container>
          <NavigationIndex contacts={"white"} />
          <ContactNavigation />
        </Container>
      );
    }
  }
}
