import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Icon
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import NavigationIndex from "./../NavigationBar/index";
import NotificationNav from "./noficationNav";
import firebase from "react-native-firebase";
import { Auth } from "./../Library/Auth";
export default class NotificationIndex extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      Notifications: [],
      MNotifications: [],
      user: {}
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("Notifications")
      .where("sendTo", "==", this.userData.email)
      .onSnapshot(doc => {
        let notif = new Array();

        doc.forEach(doc => {
          notif.push(doc.data());

          this.setState({
            Notifications: notif,
            loader: true
          });
        });
      });

    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data(),
          loader: true
        });
      });
  }

  getNotification() {
    firebase
      .firestore()
      .collection("EventInvitation_Mobile")
      .where("mobile", "==", this.state.user.mobile)
      .onSnapshot(doc => {
        let notif = new Array();

        doc.forEach(doc => {
          notif.push(doc.data());

          this.setState({
            MNotifications: notif,
            loader: true
          });
        });
      });
  }

  openNotification(linkData) {
    let data = {
      flag: "opened"
    };

    firebase
      .firestore()
      .collection("Notifications")
      .doc(linkData.invitationID)
      .update(data)
      .then(() => {
        this.props.navigation.navigate("Open", {
          params: linkData.params
        });
      });
  }

  render() {
    if (this.state.loader == false) {
      return (
        <Container>
          <NavigationIndex notifications={"white"} />
        </Container>
      );
    } else {
      return (
        <Container>
          {this.getNotification()}
          <NavigationIndex notifications={"white"} />
          <NotificationNav index={"#5067FF"} />
          <Content>
            <List>
              {this.state.Notifications.map(n => (
                <ListItem avatar>
                  <Left>
                    <Thumbnail
                      source={{
                        uri: n.sender.imageURL
                      }}
                    />
                  </Left>
                  <Body>
                    <Text note>
                      {n.sender.name.first} {n.sender.name.middle}{" "}
                      {n.sender.name.last}
                    </Text>
                    <Text numberOfLines={2}>{n.message}</Text>
                  </Body>
                  <Right>
                    <TouchableOpacity
                      onPress={() => {
                        this.openNotification(n);
                      }}
                    >
                      <Icon name={"md-open"} type={"Ionicons"} />
                    </TouchableOpacity>
                  </Right>
                </ListItem>
              ))}

              {this.state.MNotifications.map(n => (
                <ListItem avatar>
                  <Left />
                  <Body>
                    <Text note>
                      {n.sender.name} wants to invite you a event
                    </Text>
                  </Body>
                  <Right>
                    <TouchableOpacity
                      onPress={() => {
                        this.props.navigation.navigate("openEvent", {
                          params: n.eventid
                        });
                      }}
                    >
                      <Icon name={"md-open"} type={"Ionicons"} />
                    </TouchableOpacity>
                  </Right>
                </ListItem>
              ))}
            </List>
          </Content>
        </Container>
      );
    }
  }
}
