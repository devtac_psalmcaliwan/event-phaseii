import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Icon
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet, Alert } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import NavigationIndex from "./../NavigationBar/index";
import NotificationNav from "./noficationNav";
import { Generate } from "./../Library/generate";
import firebase from "react-native-firebase";
import { Auth } from "./../Library/Auth";
export default class contactNotif extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      Notifications: [],
      loader: false,
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      },
      confirm: false
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data(),
          loader: true
        });
      });
  }

  getResults() {
    firebase
      .firestore()
      .collection("ContactNotification")
      .where("sendTo", "==", this.state.user.mobile)
      .onSnapshot(doc => {
        let notif = new Array();

        doc.forEach(doc => {
          notif.push(doc.data());

          this.setState({
            Notifications: notif,
            loader: true
          });
        });
      });
  }

  createMyContact(param) {
    const id = Generate.generateID();

    let data = {
      owner: this.state.user.userID,
      contactID: id,
      hisID: param.senderID,
      mobile: param.senderName.mobile,
      email: param.senderName.email,
      name: param.senderName.name
    };

    firebase
      .firestore()
      .collection("user-contacts")
      .doc(id)
      .set(data)
      .then(() => {
        let msg = "Contact Request Accepted";
        Alert.alert(
          msg,
          "",
          [
            {
              text: "OK",
              style: "cancel"
            }
          ],
          {
            cancelable: false
          }
        );
      });
  }

  createHisContact(param) {
    const id = Generate.generateID();
    const space = " ";

    let data = {
      owner: param.senderID,
      contactID: id,
      hisID: this.state.user.userID,
      mobile: this.state.user.mobile,
      email: this.state.user.email,
      name:
        this.state.user.name.first +
        space +
        this.state.user.name.middle +
        space +
        this.state.user.name.last
    };

    firebase
      .firestore()
      .collection("user-contacts")
      .doc(id)
      .set(data)
      .then(() => {
        this.createMyContact(param);
      });
  }

  render() {
    if (this.state.loader == false) {
      return (
        <Container>
          <NavigationIndex notifications={"white"} />
          <NotificationNav contact={"#5067FF"} />
        </Container>
      );
    } else {
      return (
        <Container>
          {this.getResults()}
          <NavigationIndex notifications={"white"} />
          <NotificationNav />

          <Content>
            <List>
              {this.state.Notifications.map(n => (
                <ListItem avatar>
                  <Left />
                  <Body>
                    <Text note>{n.senderName.name} wants to connect</Text>
                  </Body>
                  <Right>
                    <TouchableOpacity
                      onPress={() => {
                        let msg = "Accept " + n.senderName.name + " Request ?";
                        Alert.alert(
                          msg,
                          "",
                          [
                            {
                              text: "OK",
                              onPress: () => this.createHisContact(n),
                              style: "cancel"
                            },
                            { text: "NO" }
                          ],
                          {
                            cancelable: false
                          }
                        );
                      }}
                    >
                      <Icon name={"md-open"} type={"Ionicons"} />
                    </TouchableOpacity>
                  </Right>
                </ListItem>
              ))}
            </List>
          </Content>
        </Container>
      );
    }
  }
}
