import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Icon
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { withNavigation } from "react-navigation";

class NotificationNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTrigger: false,
      search: ""
    };
  }

  handleSearch = () => {
    this.setState({
      searchTrigger: true
    });
    this.props.sendData((value = "Prop Passing Success"));
  };
  render() {
    return (
      <Header style={{ backgroundColor: "white" }} searchBar rounded>
        {this.state.searchTrigger ? (
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="Search"
              value={this.state.search}
              onChangeText={text => this.setState({ search: text })}
            />
            <TouchableOpacity
              onPress={() => {
                this.handleSearch();
              }}
            >
              <Icon type={"MaterialIcons"} name={"keyboard-arrow-right"} />
            </TouchableOpacity>
          </Item>
        ) : (
          <Grid>
            <Col
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("NotificationIndex");
                }}
              >
                <Icon
                  name={"event"}
                  type={"MaterialIcons"}
                  style={{
                    color: this.props.index
                  }}
                />
              </TouchableOpacity>
            </Col>

            <Col
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("contactNotif");
                }}
              >
                <Icon
                  name={"call-end"}
                  type={"MaterialIcons"}
                  style={{
                    color: this.props.contact
                  }}
                />
              </TouchableOpacity>
            </Col>
          </Grid>
        )}
      </Header>
    );
  }
}
export default withNavigation(NotificationNav);
