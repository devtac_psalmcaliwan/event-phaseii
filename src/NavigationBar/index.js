import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Icon
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { withNavigation } from "react-navigation";

class NavigationIndex extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Header>
        <Grid>
          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("EventsIndex")}
            >
              <Icon
                name={"home"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.events
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("ContactsIndex")}
            >
              <Icon
                name={"contacts"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.contacts
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate("NotificationIndex")
              }
            >
              <Icon
                name={"notifications"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.notifications
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("MenuIndex")}
            >
              <Icon
                name={"dehaze"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.menu
                }}
              />
            </TouchableOpacity>
          </Col>
        </Grid>
      </Header>
    );
  }
}
export default withNavigation(NavigationIndex);
