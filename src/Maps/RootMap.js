import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Platform,
  WebView,
  TouchableOpacity,
  Image
} from "react-native";
import firebase from "react-native-firebase";
import { throwIfEmpty } from "rxjs/operators";
import MapView, { Marker, Circle } from "react-native-maps";
import { Auth } from "./../Library/Auth";
import { Generate } from "./../Library/generate";
import {
  Container,
  Header,
  Button,
  Left,
  Body,
  Right,
  Icon,
  Text,
  Footer,
  Fab
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import { from } from "rxjs";
export default class RootMap extends Component {
  userData = Auth.getUser();
  generate = Generate.generateID();
  constructor(props) {
    super(props);

    this.state = {
      active: false,
      initialPosition: "unknown",
      load: false,
      region: {
        longitude: 125.0021795,
        latitude: 11.2284315,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      coords: {
        longitude: 125.0021795,
        latitude: 11.2284315
      },

      coords1: {
        longitude: 125.0021798,
        latitude: 11.2284315
      },

      userMarker: {
        longitude: undefined,
        latitude: undefined
      },
      user: {},
      params: {},
      member: {},
      memberLoad: false,
      markers: [],
      markerLocation: []
    };
  }

  componentDidMount() {
    let params = this.props.navigation.getParam("params");

    firebase
      .firestore()
      .collection("LocationTracking")
      .where("member", "==", this.userData.uid)
      .where("tracking", "==", true)
      .onSnapshot(doc => {
        let contact = new Array();

        doc.forEach(doc => {
          contact.push(doc.data());

          // this.setState({
          //   markerLocation: contact,
          //   ready: true
          // });
        });
      });

    if (params) {
      navigator.geolocation
        .getCurrentPosition(pos => {
          this.setState((prevState, props) => {
            return {
              load: true,
              userMarker: {
                longitude: pos.coords.longitude,
                latitude: pos.coords.latitude
              },
              region: {
                latitude: params.regionSet.lat,
                longitude: params.regionSet.lng,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
              },
              coords: {
                latitude: params.regionSet.lat,
                longitude: params.regionSet.lng
              },
              params: params
            };
          });
        })
        .then(() => {
          firebase
            .firestore()
            .collection("users")
            .doc(this.userData.uid)
            .get()
            .then(user => {
              this.setState({
                user: user.data()
              });
            });
        })
        .then(() => {
          firebase
            .firestore()
            .collection("LocationTracking")
            .where("eventID", "==", "16lBNkPwclpsHnZLYxFW")
            .onSnapshot(doc => {
              let contact = new Array();

              doc.forEach(doc => {
                contact.push(doc.data());

                this.setState({
                  markers: contact,
                  ready: true
                });
              });
            });
        });
    } else {
      navigator.geolocation.getCurrentPosition(
        position => {
          const initialPosition = JSON.stringify(position);

          this.setState({ initialPosition });
          this.setState({
            load: true,
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude
            }
          });
        },
        error => alert(error.message),
        {
          enableHighAccuracy: false,
          timeout: 20000,
          maximumAge: 1000
        }
      );
    }

    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      });
  }

  calculate() {
    const haversine = require("haversine");

    let start = {
      latitude: this.state.region.latitude,
      longitude: this.state.region.longitude
    };

    let end = {
      latitude: this.state.userMarker.latitude,
      longitude: this.state.userMarker.longitude
    };

    let distance = haversine(start, end, { unit: "meter" });

    return distance;
  }

  checkIn() {
    let result = this.calculate();
    let data = {
      eventID: this.state.params.eventID,
      member: this.userData.uid,
      docID: this.generate,
      date: new Date(),
      status: true,
      coords: this.state.userMarker
    };

    if (result <= 50) {
      firebase
        .firestore()
        .collection("EventMemberCheckIn")
        .doc(data.docID)
        .set(data)
        .then(() => {});
    } else {
    }
  }

  enableLocationTracking() {
    let data = {
      eventID: this.state.params.eventID,
      member: this.userData.uid,
      docID: this.generate,
      date: new Date(),
      tracking: true,
      coords: this.state.userMarker
    };

    try {
      firebase
        .firestore()
        .collection("LocationTracking")
        .doc(data.docID)
        .set(data)
        .then(() => {});
    } catch (error) {}
  }
  render() {
    if (this.state.load == true) {
      return (
        <Container>
          <Header>
            <Body>
              <Text style={{ color: "white" }}> Location Screen</Text>
            </Body>
            <Right>
              <Button
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                transparent
              >
                <Icon type={"MaterialIcons"} name={"clear"} />
              </Button>
            </Right>
          </Header>

          <View style={{ flex: 1, padding: 10 }}>
            <MapView
              initialRegion={this.state.region}
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0
              }}
            >
              <Circle
                center={this.state.coords}
                radius={50}
                strokeWidth={1}
                strokeColor={"#1E1E1E"}
              />

              <Marker
                style={{ position: "absolute" }}
                coordinate={this.state.coords}
              >
                <Image
                  source={{ uri: this.state.params.primaryURL }}
                  style={{ height: 20, width: 20 }}
                  resizeMode="contain"
                />
              </Marker>

              {this.state.markers.map(m => (
                <Marker coordinate={m.coords} title={m.member} />
              ))}

              {/* <Marker coordinate={this.state.userMarker} /> */}
            </MapView>
          </View>

          {this.state.params.creator == this.userData.uid ? (
            <Fab
              active={this.state.active}
              direction="up"
              style={{ backgroundColor: "#5067FF" }}
              position="bottomRight"
              onPress={() => this.setState({ active: !this.state.active })}
            >
              <Icon type="MaterialIcons" name="my-location" />

              <Button style={{ backgroundColor: "#34A34F" }}>
                <Icon type="EvilIcons" name="refresh" />
              </Button>
            </Fab>
          ) : (
            <Fab
              active={this.state.active}
              direction="up"
              style={{ backgroundColor: "#5067FF" }}
              position="bottomRight"
              onPress={() => this.setState({ active: !this.state.active })}
            >
              <Icon type="MaterialIcons" name="my-location" />

              <Button style={{ backgroundColor: "#34A34F" }}>
                <Icon type="Entypo" name="location" />
              </Button>

              <Button
                style={{ backgroundColor: "#DD5144" }}
                onPress={() => {
                  this.enableLocationTracking();
                }}
              >
                <Icon type="Entypo" name="upload-to-cloud" />
              </Button>

              <Button
                style={{ backgroundColor: "#DD5144" }}
                onPress={() => {
                  this.checkIn();
                }}
              >
                <Icon type="MaterialCommunityIcons" name="map-marker-radius" />
              </Button>
            </Fab>
          )}
        </Container>
      );
    } else {
      return null;
    }
  }
}
