import React, { Component } from "react";
import {
  Container,
  Header,
  Icon,
  Left,
  Body,
  Right,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Button,
  List,
  ListItem,
  Fab,
  Item,
  Input
} from "native-base";
import {
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  StatusBar
} from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import NavigationIndex from "./../NavigationBar/index";
import firebase from "react-native-firebase";
import EventsFeeds from "./components/feeds";
import { Linking } from "react-native";
import { LocationTracker } from "./../Library/LocationTracker";
export default class EventsIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: "false",
      loader: false,
      eventData: [],
      visible: false,
      search: "",
      command: "nogo",
      trigger: false,
      linker: false,
      linkData: ""
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("events")
      .onSnapshot(doc => {
        let events = new Array();

        doc.forEach(doc => {
          events.push(doc.data());

          this.setState({
            eventData: events,
            loader: true
          });
        });
      });

    Linking.getInitialURL()
      .then(url => {
        if (url) {
          const initLink = url;
          const link = initLink.slice(-20);

          this.checkLink(link);
        }
      })
      .catch(err => console.error("An error occurred", err));
  }

  checkLink(link) {
    firebase
      .firestore()
      .collection("EventLinks")
      .doc(link)
      .get()
      .then(res => {
        this.props.navigation.navigate("NotificationIndex");
      });
  }

  search(search) {
    firebase
      .firestore()
      .collection("events")
      .where("title", "==", search)
      .get()
      .then(eventData => {
        let events = new Array();

        eventData.forEach(eventData => {
          events.push(eventData.data());
        });

        this.setState({
          eventData: events
        });
      });
  }

  render() {
    if (this.state.loader) {
      return (
        <Container>
          {LocationTracker.checkCollection()}
          <NavigationIndex events={"white"} />
          <EventsFeeds />
        </Container>
      );
    } else {
      return (
        <Container>
          {LocationTracker.checkCollection()}
          <NavigationIndex events={"white"} />
        </Container>
      );
    }
  }
}
