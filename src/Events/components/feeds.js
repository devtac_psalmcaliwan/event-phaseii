import React, { Component } from "react";
import {
  Container,
  Header,
  Icon,
  Left,
  Fab,
  Body,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Text,
  List,
  Input,
  Button,
  Item
} from "native-base";
import { withNavigation } from "react-navigation";
import { View, Image, TouchableOpacity } from "react-native";
import firebase from "react-native-firebase";

class EventsFeeds extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: "false",
      loader: "false",
      eventData: [],
      visible: false,
      search: "",
      searchTrigger: false
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("events")
      .onSnapshot(doc => {
        let events = new Array();

        doc.forEach(doc => {
          events.push(doc.data());

          this.setState({
            eventData: events
          });
        });
      });
  }

  search(search) {
    this.setState({
      searchTrigger: true
    });
    firebase
      .firestore()
      .collection("events")
      .where("title", "==", search)
      .get()
      .then(eventData => {
        let events = new Array();

        eventData.forEach(eventData => {
          events.push(eventData.data());
        });

        this.setState({
          eventData: events
        });
      });
  }

  render() {
    return (
      <Container>
        <View>
          <Item>
            <Input
              placeholder="Search"
              value={this.state.search}
              onChangeText={text => this.setState({ search: text })}
            />

            {this.state.searchTrigger ? (
              <TouchableOpacity
                onPress={() => {
                  this.setState({ searchTrigger: false }),
                    this.componentDidMount();
                }}
              >
                <Icon type="MaterialIcons" name="cancel" />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  this.search(this.state.search);
                }}
              >
                <Icon type="Ionicons" name="md-search" />
              </TouchableOpacity>
            )}
          </Item>
        </View>
        <Content>
          {this.state.eventData.map(e => (
            <Card>
              <CardItem>
                <Left>
                  <Thumbnail
                    source={{
                      uri:
                        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEXxWiL////xVBTwSADwTgDwSwDxVxv60MbxVRb83dX1l332n4fxWR/95+HwRQD1kHT4taT++PX0h2fwUQrxXSjzdk/2pZD+9vP97Of5xbjyZDTzc0v84tv6y7/2moH5va7yaTz3rZr0hGP0fVr72M/70MXyb0X1jXDvMAD3qpb0gF/zdE3yaDvyYjL4s6H2noZSTZJsAAAGC0lEQVR4nO2a65aiOhBGJbdGRFqNInhBu6V1errHef+3OyIoFQS56Myas9a3fyoU2ZCkUoFeDwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABdUUIKmaAaHOxqmaL/eLuehliug3GCNalXVOxtmB68FtlPNrvS5BbVXoHES7BtWyn3kYhiYF2Y2LWXX8wvB79khszxL3wuH1a0+f61wGGy27uSS21381R7q4UhH1pFQ7nKA7zWBqhBHAKrnCAeTXSjcXQTM2xhqGb5wVfD/vMM2WuFX8bHjrd/juSp1Buy33/WUIzvG56u2n6stzMckGs939Ce1Ala1nzaVvFfMmROvaEVyJZRHzd83kxDDcdssVh8b2cTf13oun7LTPywoVrOLux+PZS3TEPPPaGUrYU8zC1C4LWL+rBhT+U8JlgwJL9zMhIs67vdZR43fB5Vhj1XUMPrWHBTSiLRP8oMFdPJQlWI4tRcaujmlDXbTaKl616hNbv3nCsNe2JDDAcstebu9EwxopIy/WNabuhqvhxsXvr9/svGn0lBJcsMs8vkEY2Lac5m0TF8iU/x4vXmzdlNOa+aKqoNWUQM/cRQqTAbnQE3o+j9S3bgSJcYumK6oeN6vP7iuWO7udTV3m5Dwl9CrkYVig0No5Oh+nWdYQuGOl8XlRkqozukxL+uDWqVD7V9rFiiFO96vaEekdN3yrivZjTXzQ8sMRRbY16+NvySZFsYKvlWrtfJ0KPtOl3Y/a6Kxvx7hu+kzjDwL5VSY0O9rSoSuhjyIzk7lmYJYEbTpBOWPMPKhe+BtTPU74UA41Uc94NxJ0PlfdJQSSe9Y0j6861h1hVW4SY0UuyJdJJvashMwcCfekm24J74cjZ9a97CUDG+NxrzkQyZrobn43/bXAjBhWMMybVsYagWRsjI09eUdUqOUu4rygPD8MepFad7sh2YN3vO3McMfc6y1mht/HPOqw0NjUXWeHuTG6qSvlFbhGG4jm9yTVY8dTacb8lSTDEa+lM3NjTLvD2r8KkxLCXW6ePvajjURvfRdID3ZWND+ov12WL5Wmc4di6bGF0Nf5q3253S6KKpoavpaW3q1fuGwUBe29fVsFhbcJrTktqvkaHRScM2Fcgdw9Vo6ZH7/yxD8UGu8aUaGhqdO2o+CguGwyBhuIrDozMT3BxATzOki9RdY8PiWZ0Mxz/4maTmut1fe5bhzXq3kaFxX6pSX73h3b2KP/IM93/5Gf4dw5f8v3PKb2ZIl8lO13H4dwzpXDpvnA9tujXfeS7taujFLQyNfBg3NnR75LTO+bCboeutycVrDTUpJtPtn2ZrGmMh2HVN09zQytf1tiBPsLQCNg05LRdV85U3fX/TeV1aY0hrdSdb2is5M7cobg0do0dzOiem46lhbSGNvax941345obmSPiWzGaab2kPLTe0jvx6w11vQA9uVQHbZgEceYzUS66teZP68P7evTQ2ScLoNRrd7uqV1YfzSApmK8WEDunvWUdoWuNLmjBOc0GkebIbrPWpplWTUTArV2xhaCwNKSty6fIa31r777NdZD7wTdZ9G+/T8OKm5Co8+v7bKOyfO3DF+/4Whj1RuitorbzaXYwywsv4rDPMnw33b8PkPMFQle4LhlIQw3RBRQyHx5JzTnxeJ6AaQ7rDJJZ3bt0TDHtsf/MU5++SrmnCNBcTQ9+blbRquM8zWo2hkW2U51RumX5VGNK9+9q3hIo+rqShEbfJqnj8li028pXZWvYUPxS2EeMJnfioYVx8bxH8LKQFxn+GZaMlWC/K96LcfeRcqU+jrtCvYfr90vDD36Z5QO3SGDtxaY19SAMeFmdlW06dcDVPzpr3Q2cqjUWAWuZNeFdGgMNWFla159ss99EmDs7xxuOgvx5FM5uLqs02l3wKVSt4Pl5k36CJa4Wsbj7Pyj6yun5wlJ91Shw3r+ZKvvBKA9gVOU7l4WRazT74ohgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD4v/AfbV1shTwEnyUAAAAASUVORK5CYII="
                    }}
                  />
                  <Body>
                    <Text>{e.whereToPost}</Text>
                    <Text style={{ fontSize: 10 }} note>
                      Sponsored by: {e.creatorData.name.first} {}
                      {e.creatorData.name.middle}
                      {}
                      {e.creatorData.name.last} {}
                    </Text>
                    <Text numberOfLines={2}>{e.description}</Text>
                  </Body>
                </Left>
              </CardItem>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("Open", {
                    params: e
                  })
                }
              >
                <CardItem cardBody>
                  <Image
                    source={{ uri: e.primaryURL }}
                    style={{ height: 200, width: null, flex: 1 }}
                  />
                </CardItem>
              </TouchableOpacity>

              <View
                style={{ paddingLeft: 20, paddingTop: 10, paddingBottom: 10 }}
              >
                <Text style={{ fontWeight: "bold" }}>{e.title}</Text>
              </View>

              <View
                style={{ paddingLeft: 20, paddingTop: 10, paddingBottom: 10 }}
              >
                <Text style={{ fontSize: 10 }}>
                  {e.schedule.dateFrom} - {e.schedule.dateTo}
                </Text>
                <Text style={{ fontSize: 10 }}>{e.venue}</Text>
              </View>
            </Card>
          ))}
          <List />
        </Content>
      </Container>
    );
  }
}

export default withNavigation(EventsFeeds);
