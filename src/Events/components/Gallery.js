import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text
} from "native-base";

import { ScrollView, View, Image } from "react-native";
import firebase from "react-native-firebase";
export default class Gallery extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gallery: [],
      loader: false
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("event-gallery")
      .where("eventID", "==", this.state.param.eventID)
      .onSnapshot(doc => {
        let images = new Array();

        doc.forEach(doc => {
          images.push(doc.data());

          this.setState({
            gallery: images,
            loader: true
          });
        });
      });
  }
  render() {
    if (this.state.loader == false) {
      return null;
    } else {
      return (
        <Container>
          <Content>
            <ScrollView horizontal={true}>
              {this.state.gallery.map(g => (
                <View style={{ padding: 10 }}>
                  <Image
                    source={{ uri: g.galleryURL }}
                    style={{ height: 100, width: 200, flex: 1 }}
                  />
                </View>
              ))}
            </ScrollView>
          </Content>
        </Container>
      );
    }
  }
}
