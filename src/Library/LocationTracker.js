import firebase from "react-native-firebase";

export class LocationTracker {
  static checkCollection() {
    let userData = firebase.auth().currentUser;
    if (userData) {
      setInterval(() => {
        navigator.geolocation.getCurrentPosition(pos => {
          let data = {
            coords: {
              longitude: pos.coords.longitude,
              latitude: pos.coords.latitude
            }
          };

          firebase
            .firestore()
            .collection("LocationTracking")
            .where("member", "==", userData.uid)
            .where("tracking", "==", true)
            .get()
            .then(querySnapshot => {
              querySnapshot.forEach(doc => {
                let id = doc.id;
                this.updateLocation(id, data);
              });
            })
            .catch(error => {});
        });
      }, 5000);
    }
    //create script for getting document from LocationTracker
  }

  static updateLocation(id, data) {
    firebase
      .firestore()
      .collection("LocationTracking")
      .doc(id)
      .update(data);
  }
}
