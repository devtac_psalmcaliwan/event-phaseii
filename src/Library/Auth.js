import firebase from "react-native-firebase";

export class Auth {
  static getUser() {
    return firebase.auth().currentUser;
  }

  static emailPassword(params) {
    return firebase
      .auth()
      .signInWithEmailAndPassword(params["email"], params["password"]);
  }

  static createUserEmailPassword(params) {
    return firebase
      .auth()
      .createUserWithEmailAndPassword(params.email, params.password);
  }
}
