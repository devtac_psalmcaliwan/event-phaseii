import firebase from "react-native-firebase";

export class Uploader {
  static UploadUserImage(param) {
    const sessionId = new Date().getTime();
    let file = param.path;
    return firebase
      .storage()
      .ref("user", file)
      .child(`${sessionId}.${param.mime}`)
      .putFile(file);
  }
}
