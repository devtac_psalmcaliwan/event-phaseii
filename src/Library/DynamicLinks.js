import { Generate } from "./generate";
import firebase from "react-native-firebase";

export class DynamicLink {
  static events() {
    const invitationID = Generate.generateID();
    const httpLink = "https://mobileeventmanager.page.link/invite/";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${invitationID}`;

    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    let data = {
      invitationID,
      link,
      linkID
    };

    return data;
  }
}
