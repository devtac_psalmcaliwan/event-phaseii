import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Icon,
  Text,
  Button,
  ListItem,
  Separator,
  Radio,
  ActionSheet
} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import { View, Image, ImageBackground } from "react-native";
import { from } from "rxjs";
import { Generate } from "./../../Library/generate";
import { Auth } from "./../../Library/Auth";
import firebase from "react-native-firebase";
import Spinner from "react-native-loading-spinner-overlay";
export default class GroupCreator extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      image:
        "https://firebasestorage.googleapis.com/v0/b/event-management-66d93.appspot.com/o/assets%2Fimages%2Fimg_placeholder5.jpg?alt=media&token=64cd97b1-248c-40cc-ac69-4e4ce824aabd",
      name: "",
      desciption: "",
      tagline: "",
      location: "",
      visible: false
    };
  }

  creation(imageURL) {
    console.log("|=============================Creating");
    const id = Generate.generateID();
    let data = {
      groupID: id,
      image: imageURL,
      desciption: this.state.desciption,
      tagline: this.state.tagline,
      location: this.state.location,
      active: true,
      groupName: this.state.name,
      superAdmin: this.userData.uid
    };

    firebase
      .firestore()
      .collection("Groups")
      .doc(data.groupID)
      .set(data)
      .then(() => {
        this.setState({ visible: false });
      });
  }

  Upload() {
    console.log("|---------------------------------------Upload");
    let text = Generate.generateID();
    const object = {
      id: text
    };

    const groups = "groups";

    try {
      console.log("|=============================Uploading");
      firebase
        .storage()
        .ref(`${groups}/${object.id}`)
        .child(object.id)
        .putFile(this.state.image)
        .then(url => {
          let imageURL = url.downloadURL;
          this.creation(imageURL);
        });
    } catch (error) {
      console.log("|------------------------>", error);
    }
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Group Creator </Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>

        <Content>
          <Spinner
            visible={this.state.visible}
            textContent={"Setting up group profile ... Please wait"}
            textStyle={{ color: "#FFF" }}
          />
          <View>
            <ImageBackground
              source={{
                uri: this.state.image
              }}
              style={{ height: 200, width: null, flex: 1 }}
            >
              <Button
                transparent
                onPress={() => {
                  ImagePicker.openPicker({
                    width: 300,
                    height: 400
                  }).then(primaryIMG => {
                    this.setState({
                      image: primaryIMG.path,
                      active: true
                    });
                  });
                }}
              >
                <Icon type={"Entypo"} name={"upload-to-cloud"} />
              </Button>
            </ImageBackground>
          </View>

          <Form>
            <Item fixedLabel>
              <Input
                placeholder={"Group Name/Organization"}
                value={this.state.name}
                onChangeText={text => this.setState({ name: text })}
              />
            </Item>

            <Item fixedLabel>
              <Input
                multiline={true}
                numberOfLines={10}
                placeholder={"Description"}
                value={this.state.desciption}
                onChangeText={text => this.setState({ desciption: text })}
              />
            </Item>

            <Item fixedLabel>
              <Input
                placeholder={"Tagline"}
                value={this.state.tagline}
                onChangeText={text => this.setState({ tagline: text })}
              />
            </Item>

            <Item fixedLabel>
              <Input
                placeholder={"Location"}
                value={this.state.location}
                onChangeText={text => this.setState({ location: text })}
              />
            </Item>
          </Form>

          <Content>
            <Button
              full
              onPress={() => {
                this.setState({ visible: true }), this.Upload();
              }}
            >
              <Text>Create</Text>
            </Button>
          </Content>
        </Content>
      </Container>
    );
  }
}
