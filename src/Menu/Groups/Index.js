import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Icon,
  Text,
  Button,
  ListItem,
  Separator
} from "native-base";

import { TouchableOpacity } from "react-native";
import firebase from "react-native-firebase";
import { from } from "rxjs";
export default class GroupIndex extends Component {
  constructor(props) {
    super(props);

    this.state = {
      groups: []
    };
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("Groups")
      .onSnapshot(doc => {
        let groups = new Array();

        doc.forEach(doc => {
          groups.push(doc.data());

          this.setState({
            groups: groups
          });
        });
      });
  }
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Groups </Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>
        <Content>
          {this.state.groups.map(g => (
            <ListItem>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("GroupOpen", {
                    params: g.groupID
                  });
                }}
              >
                <Text>{g.groupName}</Text>
              </TouchableOpacity>
            </ListItem>
          ))}

          <Content>
            <Button
              success
              full
              onPress={() => {
                this.props.navigation.navigate("GroupCreator");
              }}
            >
              <Text>Create +</Text>
            </Button>
          </Content>
        </Content>
      </Container>
    );
  }
}
