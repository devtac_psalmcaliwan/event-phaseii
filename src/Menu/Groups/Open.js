import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Icon,
  Text,
  Button,
  ListItem,
  Separator,
  Radio,
  ActionSheet
} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import { View, Image, ImageBackground, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { from } from "rxjs";
import { Generate } from "./../../Library/generate";
import { Auth } from "./../../Library/Auth";
import firebase from "react-native-firebase";
import Spinner from "react-native-loading-spinner-overlay";
import { Col, Row, Grid } from "react-native-easy-grid";
export default class GroupOpen extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      loader: false,
      group: {},
      privacyModal: false
    };
  }

  componentDidMount() {
    console.log(this.state.params);

    firebase
      .firestore()
      .collection("Groups")
      .doc(this.state.params)
      .get()
      .then(doc => {
        this.setState({
          group: doc.data()
        });
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Group Creator </Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>

        <Content>
          <View>
            <ImageBackground
              style={{ height: 200, width: null, flex: 1 }}
              source={{ uri: this.state.group.image }}
            />
          </View>

          <Grid style={{ backgroundColor: "#212121" }}>
            <Col style={{ width: "80%" }} />
            <Col>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("GroupMembers", {
                    params: this.state.params
                  });
                }}
              >
                <Icon
                  type={"FontAwesome"}
                  name={"user-plus"}
                  style={{ color: "#12BC00" }}
                />
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Settings", {
                    params: this.state.params
                  });
                }}
              >
                <Icon
                  type={"Entypo"}
                  name={"cog"}
                  style={{ color: "#12BC00" }}
                />
              </TouchableOpacity>
            </Col>
          </Grid>

          <View>
            <Text>Group Name: {this.state.group.groupName}</Text>
            <Text>Location: {this.state.group.location}</Text>
            <Text>Description: {this.state.group.desciption}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}
