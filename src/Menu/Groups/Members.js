import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Icon,
  Text,
  Button,
  ListItem,
  Separator,
  Radio,
  ActionSheet
} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import { View, Image, ImageBackground, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { from } from "rxjs";
import { Generate } from "./../../Library/generate";
import { Auth } from "./../../Library/Auth";
import firebase from "react-native-firebase";
import Spinner from "react-native-loading-spinner-overlay";
import { Col, Row, Grid } from "react-native-easy-grid";

import MembersNavigation from "./components/membersNavigation";
export default class GroupMembers extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      loader: false,
      group: {},
      privacyModal: false
    };
  }

  componentDidMount() {
    console.log(this.state.params);

    firebase
      .firestore()
      .collection("Groups")
      .doc(this.state.params)
      .get()
      .then(doc => {
        this.setState({
          group: doc.data()
        });
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Group Members </Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>
        <MembersNavigation params={this.state.params} members={"#3F51B5"} />
      </Container>
    );
  }
}
