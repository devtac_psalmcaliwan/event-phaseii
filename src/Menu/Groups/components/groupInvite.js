import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Icon,
  Text,
  Button,
  List,
  ListItem,
  Separator,
  Radio,
  ActionSheet,
  Thumbnail
} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import { View, Image, ImageBackground, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { from } from "rxjs";
import { Generate } from "./../../../Library/generate";
import { Auth } from "./../../../Library/Auth";
import firebase from "react-native-firebase";
import Spinner from "react-native-loading-spinner-overlay";
import { Col, Row, Grid } from "react-native-easy-grid";

import MembersNavigation from "./membersNavigation";
export default class groupInvite extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      loader: false,
      group: {},
      privacyModal: false,
      userList: []
    };
  }

  componentDidMount() {
    console.log(this.state.params);

    firebase
      .firestore()
      .collection("Groups")
      .doc(this.state.params)
      .get()
      .then(doc => {
        this.setState({
          group: doc.data()
        });
      });

    firebase
      .firestore()
      .collection("users")
      .onSnapshot(doc => {
        let users = new Array();

        doc.forEach(doc => {
          users.push(doc.data());

          this.setState({
            userList: users,
            ready: true
          });
        });
      });
  }

  createGroupInvite(params) {
    const data = {
      groupID: this.state.params,
      invitee: params.userID,
      inviteeData: params,
      inviteID: Generate.generateID()
    };

    firebase
      .firestore()
      .collection("GroupInivites")
      .doc(data.inviteID)
      .set(data)
      .then(() => {
        console.log("|-----------Invite Sent-----------|");
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Invite Members</Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>
        <MembersNavigation invite={"#3F51B5"} />

        <Content>
          <List>
            {this.state.userList.map(uL => (
              <ListItem avatar>
                <Left>
                  <Thumbnail source={{ uri: uL.imageURL }} />
                </Left>
                <Body>
                  <Text>
                    {uL.name.first} {uL.name.middle} {uL.name.last}
                  </Text>
                  <Text note>{uL.email}</Text>
                </Body>
                <Right>
                  <TouchableOpacity
                    onPress={() => {
                      this.createGroupInvite(uL);
                    }}
                  >
                    <Icon type={"Ionicons"} name={"ios-add-circle-outline"} />
                  </TouchableOpacity>
                </Right>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}
