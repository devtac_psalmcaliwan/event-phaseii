import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Icon
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import { withNavigation } from "react-navigation";
import firebase from "react-native-firebase";

class MembersNavigation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTrigger: false,
      search: "",
      platform: []
    };
  }

  render() {
    return (
      <Header style={{ backgroundColor: "white" }} searchBar rounded>
        <Grid>
          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("GroupMembers", {
                  params: this.props.params
                });
              }}
            >
              <Icon
                name={"group"}
                type={"MaterialIcons"}
                style={{
                  color: this.props.members
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity>
              <Icon
                name={"user-md"}
                type={"FontAwesome"}
                style={{
                  color: this.props.platform
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("groupInvite", {
                  params: this.props.params
                });
              }}
            >
              <Icon
                name={"user-plus"}
                type={"FontAwesome"}
                style={{
                  color: this.props.invite
                }}
              />
            </TouchableOpacity>
          </Col>

          <Col
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <TouchableOpacity>
              <Icon
                name="ios-search"
                style={{
                  color: this.props.search
                }}
              />
            </TouchableOpacity>
          </Col>
        </Grid>
      </Header>
    );
  }
}
export default withNavigation(MembersNavigation);
