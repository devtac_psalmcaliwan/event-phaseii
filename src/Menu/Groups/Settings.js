import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Icon,
  Text,
  Button,
  ListItem,
  Separator,
  Radio,
  ActionSheet,
  List
} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import { View, Image, ImageBackground, TouchableOpacity } from "react-native";
import Modal from "react-native-modal";
import { from } from "rxjs";
import { Generate } from "../../Library/generate";
import { Auth } from "../../Library/Auth";
import firebase from "react-native-firebase";
import Spinner from "react-native-loading-spinner-overlay";
import { Col, Row, Grid } from "react-native-easy-grid";

var BUTTONS = ["Open", "Closed", "Public", "Private", "Secret"];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
export default class Settings extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      loader: false,
      group: {},
      privacyModal: false,
      platformVisibility: "",
      settingsExist: false,
      settings: {
        settingsID: "",
        platformVisibility: ""
      }
    };
  }

  componentDidMount() {
    console.log(this.state.params);
    firebase
      .firestore()
      .collection("Groups")
      .doc(this.state.params)
      .onSnapshot(doc => {
        this.setState({
          group: doc.data()
        });
      });

    firebase
      .firestore()
      .collection("GroupSettings")
      .doc(this.state.params)
      .onSnapshot(doc => {
        console.log(doc.data());
        this.setState({
          settings: {
            settingsID: doc.data().settingsID,
            platformVisibility: doc.data().platformVisibility
          }
        });
      });
  }

  saveGroupSettings() {
    const data = {
      settingsID: this.state.group.groupID,
      platformVisibility: this.state.platformVisibility
    };

    firebase
      .firestore()
      .collection("GroupSettings")
      .doc(data.settingsID)
      .set(data);
  }

  updateGroupSettings() {
    const data = {
      settingsID: this.state.group.groupID,
      platformVisibility: this.state.platformVisibility
    };

    firebase
      .firestore()
      .collection("GroupSettings")
      .doc(data.settingsID)
      .set(data);
  }

  resetGroupSettings() {
    const data = {
      settingsID: this.state.group.groupID,
      platformVisibility: "Open"
    };

    firebase
      .firestore()
      .collection("GroupSettings")
      .doc(data.settingsID)
      .set(data)
      .then(() => {
        console.log(
          "|------------------------Group Settings sucessfully updated"
        );
      });
  }
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Group Setting</Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>

        <Content>
          <View>
            <Grid>
              <Col style={{ width: "80%" }} />

              <Col style={{ width: "10%" }}>
                <TouchableOpacity
                  onPress={() => {
                    this.resetGroupSettings();
                  }}
                >
                  <Icon type={"MaterialCommunityIcons"} name={"lock-reset"} />
                </TouchableOpacity>
              </Col>
              <Col style={{ width: "10%" }}>
                <TouchableOpacity
                  onPress={() => {
                    this.state.settingsExist
                      ? this.updateGroupSettings()
                      : this.saveGroupSettings();
                  }}
                >
                  <Icon
                    type={"MaterialCommunityIcons"}
                    name={"content-save-settings"}
                  />
                </TouchableOpacity>
              </Col>
            </Grid>
          </View>

          <Separator>
            <Text>Privacy</Text>
          </Separator>
          <View>
            <Grid>
              <Col style={{ width: "10%" }}>
                <Text>
                  <Icon type={"Entypo"} name={"network"} />
                </Text>
              </Col>
              <Col style={{ width: "55%" }}>
                <TouchableOpacity
                  onPress={() => {
                    ActionSheet.show(
                      {
                        options: BUTTONS,
                        cancelButtonIndex: CANCEL_INDEX,
                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                        title: "Options"
                      },
                      buttonIndex => {
                        this.setState({
                          platformVisibility: BUTTONS[buttonIndex]
                        });
                      }
                    );
                  }}
                >
                  <Text>Platform Visibity</Text>
                </TouchableOpacity>
              </Col>
              <Col style={{ width: "35%" }}>
                <Text>
                  {this.state.platformVisibility == ""
                    ? this.state.settings.platformVisibility
                    : this.state.platformVisibility}
                </Text>
              </Col>
            </Grid>
          </View>
        </Content>
      </Container>
    );
  }
}
