import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Button,
  Switch,
  Separator
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet, Alert } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import NavigationIndex from "./../../NavigationBar/index";
import { Auth } from "./../../Library/Auth";
import firebase from "react-native-firebase";
export default class ChangePassword extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: "",
        password: ""
      },

      oldStatus: "false",
      newStatus: "false",

      oldPassword: "",
      newPassword: "",
      confirmPassword: ""
    };
  }
  componentDidMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      });
  }

  checkCreads() {
    let old = this.checkOld();
    let pass = this.checkNew();

    if (old == "true" && pass == "true") {
      this.chagePassword();
    } else {
    }
  }

  checkOld() {
    let oldStatus;
    if (this.state.oldPassword == this.state.user.password) {
      oldStatus = "true";
    } else {
      oldStatus = "false";
    }

    return oldStatus;
  }

  checkNew() {
    let newStatus;
    if (this.state.newPassword == this.state.confirmPassword) {
      newStatus = "true";
    } else {
      newStatus = "false";
    }

    return newStatus;
  }

  chagePassword() {
    let user = firebase.auth().currentUser;
    user
      .updatePassword(this.state.newPassword)
      .then(() => {
        this.updateUserPassword(this.state.newPassword);
      })
      .catch(error => {});
  }

  updateUserPassword = password => {
    let data = {
      password: password
    };

    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .update(data)
      .then(() => {
        Alert.alert(
          "Password Changed",
          "",
          [
            {
              text: "OK",
              style: "cancel"
            }
          ],
          {
            cancelable: false
          }
        );
      });
  };

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Change Password</Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>

        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Old Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.oldPassword}
                onChangeText={text => this.setState({ oldPassword: text })}
              />
            </Item>
            <Item floatingLabel>
              <Label>New Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.newPassword}
                onChangeText={text => this.setState({ newPassword: text })}
              />
            </Item>
            <Item floatingLabel>
              <Label>Confirm Password</Label>
              <Input
                secureTextEntry={true}
                value={this.state.confirmPassword}
                onChangeText={text => this.setState({ confirmPassword: text })}
              />
            </Item>
          </Form>

          <View style={{ padding: 20 }}>
            <Button
              success
              full
              onPress={() => {
                this.checkCreads();
              }}
            >
              <Text>Change Password</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
