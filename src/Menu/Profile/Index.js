import React, { Component } from "react";
import Modal from "react-native-modal";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Card,
  CardItem,
  List,
  ListItem,
  Button,
  Switch,
  Separator
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet, Alert } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import NavigationIndex from "./../../NavigationBar/index";
import { Auth } from "./../../Library/Auth";
import firebase from "react-native-firebase";
import { DynamicLink } from "./../../Library/DynamicLinks";
export default class ProfileIndex extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.navigation.getParam("param"),
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: "",
        address: "",
        iceName: "",
        iceContact: "",
        iceEmail: "",
        iceRelation: ""
      },
      btnUpdate: false,

      param: "",
      first: "",
      middle: "",
      last: "",
      mobile: "",
      address: "",
      iceName: "",
      iceContact: "",
      iceEmail: "",
      iceRelation: "",

      //triggers
      iceRelationTrigger: false,
      iceEmailTrigger: false,
      iceContactTrigger: false,
      iceNameTrigger: false,
      firsttrigger: false,
      middletrigger: false,

      lasttrigger: false,
      mobiletrigger: false,
      addresstrigger: false
    };
  }
  componentDidMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.state.id == null ? this.userData.uid : this.state.id)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      });
  }

  cancelAccount() {
    var user = firebase.auth().currentUser;

    user
      .delete()
      .then(() => {
        this.props.navigation.navigate("LoginIndex");
      })
      .catch(error => {
        console.log(error);
      });
  }

  enableUpdate() {
    this.setState({
      btnUpdate: !this.state.btnUpdate,
      first: this.state.user.name.first,
      middle: this.state.user.name.middle,
      last: this.state.user.name.last,
      mobile: this.state.user.mobile,
      address: this.state.user.address,
      iceName: this.state.user.iceName,
      iceContact: this.state.user.iceContact,
      iceEmail: this.state.user.iceEmail,
      iceRelation: this.state.user.iceRelation
    });
  }

  publicModal(param) {
    this.setState({
      param: param,
      isModalVisible: !this.state.isModalVisible
    });
  }

  updateData() {
    const data = {
      name: {
        first:
          this.state.first == ""
            ? this.state.user.name.first
            : this.state.first,
        middle:
          this.state.middle == ""
            ? this.state.user.name.middle
            : this.state.middle,
        last:
          this.state.last == "" ? this.state.user.name.last : this.state.last
      },
      mobile:
        this.state.mobile == "" ? this.state.user.mobile : this.state.mobile,
      iceName:
        this.state.iceName == "" ? this.state.user.iceName : this.state.iceName,
      iceContact:
        this.state.iceContact == ""
          ? this.state.user.iceContact
          : this.state.iceContact,
      iceRelation:
        this.state.iceRelation == ""
          ? this.state.user.iceRelation
          : this.state.iceRelation,
      iceEmail:
        this.state.iceEmail == ""
          ? this.state.user.iceEmail
          : this.state.iceEmail,
      address:
        this.state.address == "" ? this.state.user.address : this.state.address
    };

    console.log(data);

    firebase
      .firestore()
      .collection("users")
      .doc(this.state.user.userID)
      .update(data)
      .then(() => {
        this.setState({ btnUpdate: !this.state.btnUpdate }),
          this.componentDidMount();
      });
  }

  smsInvite(user) {
    console.log("------------------------------>SMS inivite");
    let data = DynamicLink.events();
    let link = data.link;
    let linkID = data.linkID;
    let invitationID = data.invitationID;

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        try {
          let data = {
            linkURL,
            linkID,
            sendTo: user.mobile,
            invitationID,
            status: "unused"
          };

          firebase
            .firestore()
            .collection("MobileLinks")
            .doc(data.invitationID)
            .set(data)
            .then(() => {
              Alert.alert(
                "SMS Sent",
                "",
                [
                  {
                    text: "OK"
                  }
                ],
                { cancelable: true }
              );
            });
        } catch (error) {
          console.log(error);
        }
      });
  }

  emailInvite(user) {
    let data = DynamicLink.events();
    let link = data.link;
    let linkID = data.linkID;
    let invitationID = data.invitationID;

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        try {
          let data = {
            linkURL,
            linkID,
            sendTo: user.email,
            invitationID,
            message: "Contact Invite",
            status: "unused"
          };

          firebase
            .firestore()
            .collection("EmailLinks")
            .doc(data.invitationID)
            .set(data)
            .then(() => {
              Alert.alert(
                "Email Sent",
                "",
                [
                  {
                    text: "OK"
                  }
                ],
                { cancelable: true }
              );
            });
        } catch (error) {
          console.log(error);
        }
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            {this.state.id != null ? (
              <Text style={{ color: "white" }}>View Profile</Text>
            ) : (
              <Text style={{ color: "white" }}>My Profile</Text>
            )}
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"cancel"} />
            </Button>
          </Right>
        </Header>
        <Content>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              padding: 10,
              backgroundColor: "gray"
            }}
          >
            <Image
              source={{ uri: this.state.user.imageURL }}
              style={{
                height: 150,
                width: 150,
                borderRadius: 150
              }}
            />
          </View>

          <List>
            <Separator style={{ backgroundColor: "#1E1E1E" }}>
              {this.state.id != null ? (
                <Grid>
                  <Col />
                  <Col />
                  <Col />
                  <Col />
                  <Col>
                    <Button
                      danger
                      transparent
                      onPress={() => {
                        this.emailInvite(this.state.user);
                      }}
                    >
                      <Icon type={"Ionicons"} name={"md-mail"} />
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      success
                      transparent
                      onPress={() => {
                        this.smsInvite(this.state.user);
                      }}
                    >
                      <Icon type={"Entypo"} name={"message"} />
                    </Button>
                  </Col>
                </Grid>
              ) : (
                <Grid>
                  <Col />
                  <Col />
                  <Col />
                  <Col />
                  <Col>
                    <Button
                      danger
                      transparent
                      onPress={() => {
                        Alert.alert(
                          "Account will be DELETED",
                          "Continue?",
                          [
                            {
                              text: "Cancel",
                              onPress: () => console.log("Cancel Pressed"),
                              style: "cancel"
                            },
                            { text: "OK", onPress: () => this.cancelAccount() }
                          ],
                          { cancelable: true }
                        );
                      }}
                    >
                      <Icon type={"MaterialIcons"} name={"not-interested"} />
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      success
                      transparent
                      onPress={() => {
                        this.enableUpdate();
                      }}
                    >
                      <Icon type={"MaterialIcons"} name={"edit"} />
                    </Button>
                  </Col>
                </Grid>
              )}
            </Separator>
            <Separator>
              <Text>Personal Information</Text>
            </Separator>

            <ListItem>
              <Left>
                <Text>First </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.first}
                      onChangeText={text => this.setState({ first: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.name.first}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <ListItem>
              <Left>
                <Text>Middle </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.middle}
                      onChangeText={text => this.setState({ middle: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.name.middle}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <ListItem>
              <Left>
                <Text>Surname </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.last}
                      onChangeText={text => this.setState({ last: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.name.last}</Text>
                )}
              </Body>

              <Right />
            </ListItem>
            <Separator>
              <Text>Contact Information</Text>
            </Separator>
            <ListItem>
              <Left>
                <Text>Mobile </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.mobile}
                      onChangeText={text => this.setState({ mobile: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.mobile}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <ListItem>
              <Left>
                <Text>Email </Text>
              </Left>
              <Body>
                <Text numberOfLines={1}>{this.state.user.email}</Text>
              </Body>
              <Right />
            </ListItem>

            <Separator>
              <Text>Mailing/Home Address</Text>
            </Separator>
            <ListItem>
              <Left>
                <Text>Address </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.address}
                      onChangeText={text => this.setState({ address: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.address}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <Separator>
              <Text>Incase of Emergency</Text>
            </Separator>

            <ListItem>
              <Left>
                <Text>Name </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.iceName}
                      onChangeText={text => this.setState({ iceName: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.iceName}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <ListItem>
              <Left>
                <Text>Contact </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.iceContact}
                      onChangeText={text => this.setState({ iceContact: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.iceContact}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <ListItem>
              <Left>
                <Text>Email </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.iceEmail}
                      onChangeText={text => this.setState({ iceEmail: text })}
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.iceEmail}</Text>
                )}
              </Body>

              <Right />
            </ListItem>

            <ListItem>
              <Left>
                <Text>Relationship </Text>
              </Left>
              <Body>
                {this.state.btnUpdate ? (
                  <Item>
                    <Input
                      value={this.state.iceRelation}
                      onChangeText={text =>
                        this.setState({ iceRelation: text })
                      }
                    />
                  </Item>
                ) : (
                  <Text>{this.state.user.iceRelation}</Text>
                )}
              </Body>

              <Right />
            </ListItem>
          </List>

          <View style={{ padding: 20 }}>
            {this.state.btnUpdate ? (
              <Button
                success
                full
                onPress={() => {
                  this.updateData();
                }}
              >
                <Text>Update</Text>
              </Button>
            ) : null}
          </View>
        </Content>
      </Container>
    );
  }
}
