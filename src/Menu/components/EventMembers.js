import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Text,
  Icon,
  Button,
  Fab,
  Item,
  Input,
  Right,
  Left,
  Body,
  Thumbnail,
  Card,
  CardItem,
  Form,
  Label
} from "native-base";

import { Col, Row, Grid } from "react-native-easy-grid";
import { TouchableOpacity, View, Image, Alert } from "react-native";
import firebase from "react-native-firebase";

import Modal from "react-native-modal";

import { Auth } from "./../../Library/Auth";
import { DynamicLink } from "./../../Library/DynamicLinks";
import { Generate } from "./../../Library/generate";

export default class EventMembers extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);
    this.state = {
      active: false,
      params: this.props.navigation.getParam("params"),
      going: 0,
      maybe: 0,
      not: 0,
      smsTrigger: false,
      goingPress: true,
      maybePress: false,
      notPress: false,
      sent: false,
      goingDATA: [],
      maybeDATA: [],
      notDATA: [],
      isModalVisible: false,
      linkEmail: "",
      linkMessage: "",
      userFlag: "",
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      },
      dateRegistered: "",
      uid: "",
      username: "",
      imgURL: ""
    };

    this.RSVPgoing = this.RSVPgoing.bind(this);
  }

  componentDidMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      });

    firebase
      .firestore()
      .collection("rsvp")
      .where("eventID", "==", this.state.params.eventID)
      .where("rsvp", "==", "going")
      .get()
      .then(res => {
        this.setState({
          going: res.size
        });
      });

    firebase
      .firestore()
      .collection("rsvp")
      .where("eventID", "==", this.state.params.eventID)
      .where("rsvp", "==", "going")
      .get()
      .then(goingDATA => {
        let events = new Array();

        goingDATA.forEach(goingDATA => {
          events.push(goingDATA.data());
        });

        this.setState({
          goingDATA: events
        });
      });

    firebase
      .firestore()
      .collection("rsvp")
      .where("eventID", "==", this.state.params.eventID)
      .where("rsvp", "==", "maybe")
      .get()
      .then(res => {
        this.setState({
          maybe: res.size
        });
      });

    firebase
      .firestore()
      .collection("rsvp")
      .where("eventID", "==", this.state.params.eventID)
      .where("rsvp", "==", "maybe")
      .get()
      .then(maybeDATA => {
        let events = new Array();

        maybeDATA.forEach(maybeDATA => {
          events.push(maybeDATA.data());
        });

        this.setState({
          maybeDATA: events
        });
      });

    firebase
      .firestore()
      .collection("rsvp")
      .where("eventID", "==", this.state.params.eventID)
      .where("rsvp", "==", "not going")
      .get()
      .then(res => {
        this.setState({
          not: res.size
        });
      });

    firebase
      .firestore()
      .collection("rsvp")
      .where("eventID", "==", this.state.params.eventID)
      .where("rsvp", "==", "not going")
      .get()
      .then(notDATA => {
        let events = new Array();

        notDATA.forEach(notDATA => {
          events.push(notDATA.data());
        });

        this.setState({
          notDATA: events
        });
      });
  }

  removeMember(rsvpID, userID) {
    Alert.alert(
      "You are about to remove this person from the event",
      "Continue?",
      [
        {
          text: "Cancel",

          style: "cancel"
        },
        {
          text: "Yes",
          onPress: () => {
            firebase
              .firestore()
              .collection("rsvp")
              .doc(rsvpID)
              .delete()
              .then(() => {
                this.componentDidMount();
              });
          }
        }
      ],
      { cancelable: false }
    );
  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });

  _toggleSMSModal = () => this.setState({ smsTrigger: !this.state.smsTrigger });

  checkExistingEmail(email, message) {
    firebase
      .firestore()
      .collection("users")
      .where("email", "==", email)
      .get()
      .then(res => {
        let size = res.size;
        let exist = "existing";
        let newer = "new";
        if (size > 0) {
          this.sendEmailLink(
            this.state.linkEmail,
            this.state.linkMessage,
            exist
          );
        } else {
          this.sendEmailLink(
            this.state.linkEmail,
            this.state.linkMessage,
            newer
          );
        }
      });
  }

  sendEmailLink(email, message, userFlag) {
    let data = DynamicLink.events();
    let link = data.link;
    let linkID = data.linkID;
    let invitationID = data.invitationID;
    let sendTo = email;
    this._toggleModal();
    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        try {
          let data = {
            linkURL,
            linkID,
            params: this.state.params,
            sendTo,
            message,
            sender: this.state.user,
            invitationID,
            userFlag
          };

          this.saveLink(data);
        } catch (error) {}
      });
  }

  convertToE164formart(mobile) {
    let sliced = mobile.slice(1);
    let e164 = "+63" + sliced;
    return e164;
  }

  sendSMSLink(mobile, message) {
    let e164 = this.convertToE164formart(mobile);

    let data = DynamicLink.events();
    let link = data.link;
    let linkID = data.linkID;
    let invitationID = data.invitationID;
    let sendTo = e164;
    let userFlag = "mobile";
    this._toggleSMSModal();
    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;

        let data = {
          linkURL,
          linkID,
          params: this.state.params,
          sendTo,
          message,
          sender: this.state.user,
          invitationID,
          userFlag
        };
        try {
          this.saveLink(data);
        } catch (error) {}
      });
  }

  saveLink(placeholder) {
    this.setState({
      isModalVisible: false
    });
    let data = {
      params: this.state.params,
      linkURL: placeholder.linkURL,
      invitationID: placeholder.invitationID,
      linkID: placeholder.linkID,
      sendTo: placeholder.sendTo,
      message: placeholder.message,
      sender: placeholder.sender,
      userFlag: placeholder.userFlag,
      flag: "available"
    };

    this.setState({
      sent: true
    });

    firebase
      .firestore()
      .collection("Notifications")
      .doc(data.invitationID)
      .set(data);

    firebase
      .firestore()
      .collection("EventLinks")
      .doc(data.invitationID)
      .set(data);
  }

  RSVPgoing() {
    if (this.state.goingPress == true) {
      return (
        <List>
          {this.state.goingDATA.map(gD => (
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  circular
                  source={{
                    uri: gD.userData.imageURL
                  }}
                />
              </Left>
              <Body>
                <Text>
                  {gD.userData.name.first} {gD.userData.name.last}
                </Text>
              </Body>

              <Right>
                <Button
                  transparent
                  danger
                  onPress={() => {
                    this.removeMember(gD.rsvpID, gD.userID);
                  }}
                >
                  <Text>Remove</Text>
                </Button>
              </Right>
            </ListItem>
          ))}
        </List>
      );
    }

    if (this.state.maybePress == true) {
      return (
        <List>
          <ListItem itemDivider>
            <Text>MAYBE</Text>
          </ListItem>

          {this.state.maybeDATA.map(Md => (
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  circular
                  source={{
                    uri: Md.user.imgURL
                  }}
                />
              </Left>
              <Body>
                <Text>
                  {Md.user.name.firstName} {""} {Md.user.name.lastName}
                </Text>

                <Text note>{Md.user.username}</Text>
              </Body>

              <Right />
            </ListItem>
          ))}
        </List>
      );
    }

    if (this.state.notPress == true) {
      return (
        <List>
          {this.state.notDATA.map(nD => (
            <ListItem thumbnail>
              <Left>
                <Thumbnail
                  circular
                  source={{
                    uri: nD.userData.imageURL
                  }}
                />
              </Left>
              <Body>
                <Text>
                  {nD.userData.name.first} {nD.userData.name.last}
                </Text>
              </Body>

              <Right />
            </ListItem>
          ))}
        </List>
      );
    }
  }

  showSentMessage() {
    if (this.state.sent == true) {
      return (
        <Content>
          <Button
            full
            success
            onPress={() => {
              this.setState({ sent: false });
            }}
          >
            <Text>Message Sent</Text>
          </Button>
        </Content>
      );
    } else {
      return null;
    }
  }

  eventNotification() {
    firebase
      .firestore()
      .collection("Notifications")
      .set(data);
  }

  rsvpActionGoing(rsvp) {
    let id = Generate.generateID();
    let data = {
      eventID: this.state.params.eventID,
      rsvpID: id,
      userID: this.userData.uid,
      userData: this.state.user,
      rsvp: rsvp
    };

    firebase
      .firestore()
      .collection("rsvp")
      .doc(id)
      .set(data)
      .then(() => {
        this.componentDidMount();
      });
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Event Members</Text>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.goBack();
              }}
            >
              <Icon
                type="MaterialIcons"
                name="clear"
                style={{ color: "white" }}
              />
            </TouchableOpacity>
          </Right>
        </Header>
        <Content>
          <View style={{ flex: 1 }}>
            <Modal isVisible={this.state.isModalVisible}>
              <Card>
                <CardItem>
                  <Item>
                    <Input
                      placeholder="Email"
                      value={this.state.linkEmail}
                      onChangeText={text => this.setState({ linkEmail: text })}
                    />
                  </Item>
                </CardItem>

                <CardItem>
                  <Item>
                    <Input
                      placeholder="Message"
                      value={this.state.linkMessage}
                      onChangeText={text =>
                        this.setState({ linkMessage: text })
                      }
                    />
                  </Item>
                </CardItem>
                <CardItem>
                  <Body>
                    <Button
                      transparent
                      success
                      onPress={() => {
                        this.checkExistingEmail(
                          this.state.linkEmail,
                          this.state.linkMessage
                        );
                      }}
                    >
                      <Text>Send</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Button transparent danger onPress={this._toggleModal}>
                      <Text>Cancel</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
            </Modal>
          </View>

          <View style={{ flex: 1 }}>
            <Modal isVisible={this.state.smsTrigger}>
              <Card>
                <CardItem>
                  <Item>
                    <Input
                      placeholder="Mobile"
                      value={this.state.linkSMS}
                      onChangeText={text => this.setState({ linkSMS: text })}
                    />
                  </Item>
                </CardItem>

                <CardItem>
                  <Item>
                    <Input
                      placeholder="Message"
                      value={this.state.linkMessage}
                      onChangeText={text =>
                        this.setState({ linkMessage: text })
                      }
                    />
                  </Item>
                </CardItem>
                <CardItem>
                  <Body>
                    <Button
                      transparent
                      success
                      onPress={() => {
                        this.sendSMSLink(
                          this.state.linkSMS,
                          this.state.linkMessage
                        );
                      }}
                    >
                      <Text>Send</Text>
                    </Button>
                  </Body>
                  <Right>
                    <Button transparent danger onPress={this._toggleSMSModal}>
                      <Text>Cancel</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
            </Modal>
          </View>

          <Card>
            <View style={{ marginTop: -20 }}>
              <Grid>
                <Col
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",

                    height: 100
                  }}
                >
                  <Text>{this.state.going}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.rsvpActionGoing((rsvp = "going"));
                    }}
                  >
                    <Icon
                      type="Ionicons"
                      name="md-thumbs-up"
                      style={{ color: "green" }}
                    />
                  </TouchableOpacity>
                </Col>

                <Col
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",

                    height: 100
                  }}
                >
                  <Text>{this.state.not}</Text>
                  <TouchableOpacity
                    onPress={() => {
                      this.rsvpActionGoing((rsvp = "not going"));
                    }}
                  >
                    <Icon
                      type="Ionicons"
                      name="md-thumbs-down"
                      style={{ color: "red" }}
                    />
                  </TouchableOpacity>
                </Col>
              </Grid>
            </View>
          </Card>
          <List>
            <ListItem itemDivider>
              <Grid>
                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        goingPress: true,
                        maybePress: false,
                        notPress: false
                      });
                    }}
                  >
                    <Text>GOING</Text>
                  </TouchableOpacity>
                </Col>
                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        goingPress: false,
                        maybePress: false,
                        notPress: true
                      });
                    }}
                  >
                    <Text>NOT GOING</Text>
                  </TouchableOpacity>
                </Col>
              </Grid>
            </ListItem>
          </List>
          {this.RSVPgoing()}
          {this.showSentMessage()}
        </Content>

        {this.state.user.userID == this.state.params.creator ? (
          <Fab
            active={this.state.active}
            direction="up"
            style={{ backgroundColor: "#5067FF" }}
            position="bottomRight"
            onPress={() => this.setState({ active: !this.state.active })}
          >
            <Icon type="MaterialIcons" name="person-add" />
            <Button
              style={{ backgroundColor: "#34A34F" }}
              onPress={this._toggleSMSModal}
            >
              <Icon name="logo-whatsapp" />
            </Button>

            <Button
              style={{ backgroundColor: "#DD5144" }}
              onPress={this._toggleModal}
            >
              <Icon name="mail" />
            </Button>

            <Button
              style={{ backgroundColor: "#007ACC" }}
              onPress={() => {
                this.props.navigation.navigate("RSVPContacts", {
                  params: this.state.params.eventID
                });
              }}
            >
              <Icon name="contacts" type="MaterialIcons" />
            </Button>
          </Fab>
        ) : null}
      </Container>
    );
  }
}
