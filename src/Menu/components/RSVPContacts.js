import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Body,
  Left,
  Right,
  Text,
  Icon,
  Button,
  List,
  ListItem
} from "native-base";
import { TouchableOpacity, Alert } from "react-native";
import { Auth } from "./../../Library/Auth";
import { Generate } from "./../../Library/generate";
import firebase from "react-native-firebase";
export default class RSVPContacts extends Component {
  userData = Auth.getUser();

  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      userData: this.props.navigation.getParam("user"),
      active: false,
      isModalVisible: false,
      smsTrigger: false,
      sent: false,
      contacts: [],
      ready: false,
      phoneTrigger: false,
      phoneNumber: [],
      area: "",
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      },
      loader: false,
      contacts: []
    };

    this.getResults = this.getResults.bind(this);
  }

  componentWillMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data(),
          ready: true
        });
      });
  }

  getResults() {
    firebase
      .firestore()
      .collection("user-contacts")
      .where("owner", "==", this.state.user.userID)
      .onSnapshot(doc => {
        let contact = new Array();

        doc.forEach(doc => {
          contact.push(doc.data());

          this.setState({
            contacts: contact,
            ready: true
          });
        });
      });
  }

  sendInvite(param) {
    const id = Generate.generateID();

    const httpLink = "https://mobileeventmanager.page.link/invite/eventid=";
    const domainLink = "mobileeventmanager.page.link";

    const linkID = `${httpLink}${id}`;
    const link = new firebase.links.DynamicLink(linkID, domainLink).android
      .setPackageName("com.eventmanagement.android")
      .ios.setBundleId("com.eventmanagement.ios");

    firebase
      .links()
      .createShortDynamicLink(link)
      .then(url => {
        const linkURL = url;
        const space = " ";
        let data = {
          docID: id,
          senderID: this.state.user.userID,
          eventid: this.state.params,
          linkID: linkID,
          mobile: param.mobile,
          linkURL: linkURL,
          opened: false,
          sender: {
            name:
              this.state.user.name.first +
              space +
              this.state.user.name.middle +
              space +
              this.state.user.name.last,
            email: this.state.user.email,
            mobile: this.state.user.mobile
          }
        };

        firebase
          .firestore()
          .collection("EventInvitation_Mobile")
          .doc(id)
          .set(data)
          .then(() => {
            this.createEventNotification(data);
          });
      });
  }

  createEventNotification(data) {
    firebase
      .firestore()
      .collection("EventNotification")
      .doc()
      .set(data)
      .then(() => {
        let msg = "Invitation Sent";
        Alert.alert(
          msg,
          "",
          [
            {
              text: "OK",
              style: "cancel"
            }
          ],
          {
            cancelable: false
          }
        );
      });
  }

  render() {
    if (this.state.ready) {
      return (
        <Container>
          <Header>
            <Body>
              <Text style={{ color: "white" }}>My Contacts</Text>
            </Body>
            <Right>
              <Button
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                transparent
              >
                <Icon type={"MaterialIcons"} name={"clear"} />
              </Button>
            </Right>
          </Header>
          {this.getResults()}
          <Content>
            <List>
              {this.state.contacts.map(c => (
                <ListItem>
                  <Body>
                    <Text>{c.name}</Text>
                  </Body>
                  <Right>
                    <TouchableOpacity
                      onPress={() => {
                        this.sendInvite(c);
                      }}
                    >
                      <Icon name="md-add-circle" type="Ionicons" />
                    </TouchableOpacity>
                  </Right>
                </ListItem>
              ))}
            </List>
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <Header>
            <Body>
              <Text style={{ color: "white" }}>My Contacts</Text>
            </Body>
            <Right>
              <Button
                onPress={() => {
                  this.props.navigation.goBack();
                }}
                transparent
              >
                <Icon type={"MaterialIcons"} name={"clear"} />
              </Button>
            </Right>
          </Header>
        </Container>
      );
    }
  }
}
