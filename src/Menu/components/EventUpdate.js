import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Left,
  Right,
  Body,
  Button,
  Text,
  Icon,
  Card,
  CardItem,
  Thumbnail,
  List,
  ListItem,
  Separator
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import { GoogleAutoComplete } from "react-native-google-autocomplete";
import LocationItem from "./../../Menu/CreateEvents/locationItem";
import {
  Image,
  View,
  DatePickerAndroid,
  TimePickerAndroid,
  TouchableOpacity,
  ScrollView
} from "react-native";
import Modal from "react-native-modal";
import firebase from "react-native-firebase";
export default class EventUpdate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      params: this.props.navigation.getParam("params"),
      titleModalVisible: false,
      descriptionModalVisible: false,
      mapsVisible: false,
      newTitle: "",
      newDescription: "",
      newDateFrom: "",
      newDateTo: "",
      newTimeFrom: "",
      newTimeTo: "",
      location: "",
      regionSet: {
        lat: "",
        lng: ""
      }
    };
    this.setPlaceQuery = this.setPlaceQuery.bind(this);
  }

  setPlaceQuery(description, res = false) {
    this.setState({
      queryPlace: description,
      location: description,
      regionSet: {
        lat: res.geometry.location.lat,
        lng: res.geometry.location.lng
      }
    });
  }

  updateEvent() {
    let data = {
      title:
        this.state.newTitle == ""
          ? this.state.params.title
          : this.state.newTitle,
      description:
        this.state.newDescription == ""
          ? this.state.params.description
          : this.state.newDescription,
      schedule: {
        dateFrom:
          this.state.newDateFrom == ""
            ? this.state.params.schedule.dateFrom
            : this.state.newDateFrom,
        dateTo:
          this.state.newDateTo == ""
            ? this.state.params.schedule.dateTo
            : this.state.newDateTo,

        timeFrom:
          this.state.newTimeFrom == ""
            ? this.state.params.schedule.newTimeFrom
            : this.state.newTimeFrom,
        timeTo:
          this.state.newTimeTo == ""
            ? this.state.params.schedule.newTimeTo
            : this.state.newTimeTo
      },
      venue:
        this.state.location == ""
          ? this.state.params.venue
          : this.state.location,
      regionSet: {
        lat:
          this.state.regionSet.lat == ""
            ? this.state.params.regionSet.lat
            : this.state.regionSet.lat,
        lng:
          this.state.regionSet.lng == ""
            ? this.state.params.regionSet.lng
            : this.state.regionSet.lng
      }
    };
    firebase
      .firestore()
      .collection("events")
      .doc(this.state.params.eventID)
      .update(data)
      .then(() => {
        this.props.navigation.navigate("ViewEvent", (params = data));
      });
  }

  _toggleTitleModal = () =>
    this.setState({ titleModalVisible: !this.state.titleModalVisible });

  _toggleDescriptionModal = () =>
    this.setState({
      descriptionModalVisible: !this.state.descriptionModalVisible
    });

  async datePickerModalFrom() {
    try {
      var today = new Date();

      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(),
        minDate: new Date(),
        mode: "spinner"
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({
          newDateFrom: day + "/" + month + "/" + year
        });
      }
    } catch ({ code, message }) {
      alert("Cannot open date picker: " + message);
    }
  }

  async timePickerModalFrom() {
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: 0,
        minute: 0,
        is24Hour: false
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        this.setState({ newTimeFrom: hour + ":" + minute });
      }
    } catch ({ code, message }) {
      console.warn("Cannot open time picker", message);
    }
  }

  async datePickerModalTo() {
    try {
      var today = new Date();

      const { action, year, month, day } = await DatePickerAndroid.open({
        date: new Date(),
        minDate: new Date(),
        mode: "spinner"
      });
      if (action !== DatePickerAndroid.dismissedAction) {
        this.setState({
          newDateTo: day + "/" + month + "/" + year
        });
      }
    } catch ({ code, message }) {
      alert("Cannot open date picker: " + message);
    }
  }

  async timePickerModalTo() {
    try {
      const { action, hour, minute } = await TimePickerAndroid.open({
        hour: 0,
        minute: 0,
        is24Hour: false
      });
      if (action !== TimePickerAndroid.dismissedAction) {
        this.setState({ newTimeTo: hour + ":" + minute });
      }
    } catch ({ code, message }) {
      console.warn("Cannot open time picker", message);
    }
  }

  titleEventCancel() {
    this.setState({ newTitle: "" });
    this._toggleTitleModal();
  }

  descEventCancel() {
    this.setState({ newDescription: "" });
    this._toggleDescriptionModal();
  }

  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Text style={{ color: "white" }}>Edit Event Details</Text>
          </Body>
          <Right>
            <Button
              onPress={() => {
                this.props.navigation.goBack();
              }}
              transparent
            >
              <Icon type={"MaterialIcons"} name={"clear"} />
            </Button>
          </Right>
        </Header>

        {/* modal area */}
        {/* Title Modal */}
        <Modal isVisible={this.state.titleModalVisible}>
          <Card>
            <CardItem>
              <Item>
                <Input
                  placeholder="Title"
                  value={this.state.newTitle}
                  onChangeText={text => this.setState({ newTitle: text })}
                />
              </Item>
            </CardItem>
            <CardItem>
              <Body>
                <Button transparent success onPress={this._toggleTitleModal}>
                  <Text>Okay</Text>
                </Button>
              </Body>
              <Right>
                <Button
                  transparent
                  danger
                  onPress={() => {
                    this.titleEventCancel();
                  }}
                >
                  <Text>Cancel</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Modal>
        {/* Description Modal */}
        <Modal isVisible={this.state.descriptionModalVisible}>
          <Card>
            <CardItem>
              <Item>
                <Input
                  placeholder="Description"
                  value={this.state.newDescription}
                  onChangeText={text => this.setState({ newDescription: text })}
                />
              </Item>
            </CardItem>
            <CardItem>
              <Body>
                <Button
                  transparent
                  success
                  onPress={this._toggleDescriptionModal}
                >
                  <Text>Okay</Text>
                </Button>
              </Body>
              <Right>
                <Button
                  transparent
                  danger
                  onPress={() => {
                    this.descEventCancel();
                  }}
                >
                  <Text>Cancel</Text>
                </Button>
              </Right>
            </CardItem>
          </Card>
        </Modal>

        {/* modal area */}

        <Content>
          <Card>
            <CardItem cardBody>
              <Image
                source={{ uri: this.state.params.primaryURL }}
                style={{ height: 200, width: null, flex: 1 }}
              />
            </CardItem>
          </Card>

          <List>
            <Separator bordered />

            {/* Title List */}
            <ListItem icon>
              <Body>
                <Text>
                  Title{" "}
                  {this.state.newTitle == ""
                    ? this.state.params.title
                    : this.state.newTitle}{" "}
                </Text>
              </Body>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.setState({ titleModalVisible: true });
                  }}
                >
                  <Icon type={"MaterialIcons"} name="input" />
                </Button>
              </Right>
            </ListItem>
            {/* Description List */}
            <ListItem icon>
              <Body>
                <Text numberOfLines={2}>
                  Description{" "}
                  {this.state.newDescription == ""
                    ? this.state.params.description
                    : this.state.newDescription}
                </Text>
              </Body>
              <Right>
                <Button
                  transparent
                  onPress={() => {
                    this.setState({ descriptionModalVisible: true });
                  }}
                >
                  <Icon type={"MaterialIcons"} name="input" />
                </Button>
              </Right>
            </ListItem>

            <Separator bordered>
              <Text>Event Schedule</Text>
            </Separator>

            {/* datePickerModalFrom */}
            <ListItem>
              <Grid>
                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      this.datePickerModalFrom();
                    }}
                  >
                    <Text>
                      {this.state.newDateFrom == ""
                        ? this.state.params.schedule.dateFrom
                        : this.state.newDateFrom}
                    </Text>
                  </TouchableOpacity>
                </Col>

                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      this.timePickerModalFrom();
                    }}
                  >
                    <Text>
                      {this.state.newTimeFrom == ""
                        ? this.state.params.schedule.timeFrom
                        : this.state.newTimeFrom}
                    </Text>
                  </TouchableOpacity>
                </Col>
              </Grid>
            </ListItem>

            {/* datePickerModalTo */}
            <ListItem>
              <Grid>
                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      this.datePickerModalTo();
                    }}
                  >
                    <Text>
                      {" "}
                      {this.state.newDateFrom == ""
                        ? this.state.params.schedule.dateTo
                        : this.state.newDateTo}
                    </Text>
                  </TouchableOpacity>
                </Col>

                <Col>
                  <TouchableOpacity
                    onPress={() => {
                      this.timePickerModalTo();
                    }}
                  >
                    <Col>
                      <Text>
                        {" "}
                        {this.state.newTimeTo == ""
                          ? this.state.params.schedule.timeTo
                          : this.state.newTimeTo}
                      </Text>
                    </Col>
                  </TouchableOpacity>
                </Col>
              </Grid>
            </ListItem>

            <Separator bordered>
              <Text>Event Venue / Location</Text>
            </Separator>

            {this.state.mapsVisible == false ? (
              <ListItem>
                <Body>
                  <Text numberOfLines={2}>
                    {this.state.location == ""
                      ? this.state.params.venue
                      : this.state.location}
                  </Text>
                </Body>
                <Right>
                  <Button
                    transparent
                    onPress={() => {
                      this.setState({ mapsVisible: true });
                    }}
                  >
                    <Icon type={"MaterialIcons"} name="edit" />
                  </Button>
                </Right>
              </ListItem>
            ) : (
              <ListItem>
                <Body>
                  <GoogleAutoComplete
                    apiKey={"AIzaSyCEUwnhKcAvv9y7notB-VQWuXl4zQlct08"}
                    debounce={200}
                    minLength={2}
                  >
                    {({
                      handleTextChange,
                      locationResults,
                      fetchDetails,
                      isSearching,
                      inputValue,
                      clearSearchs
                    }) => (
                      <React.Fragment>
                        <Item>
                          <Input
                            onChangeText={handleTextChange}
                            value={
                              this.state.queryPlace != null
                                ? this.state.queryPlace
                                : null
                            }
                          />
                        </Item>

                        <ScrollView>
                          {locationResults.map(el => (
                            <LocationItem
                              triggerParentElement={this.setPlaceQuery}
                              clearSearchResutls={clearSearchs}
                              {...el}
                              key={el.id}
                              fetchDetails={fetchDetails}
                            />
                          ))}
                        </ScrollView>
                      </React.Fragment>
                    )}
                  </GoogleAutoComplete>
                </Body>
                <Right>
                  <Button
                    transparent
                    onPress={() => {
                      this.setState({ mapsVisible: false });
                    }}
                  >
                    <Icon type={"MaterialIcons"} name="done" />
                  </Button>
                </Right>
              </ListItem>
            )}
          </List>

          <View style={{ padding: 20 }}>
            <Button
              success
              full
              onPress={() => {
                this.updateEvent();
              }}
            >
              <Text> Update</Text>
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}
