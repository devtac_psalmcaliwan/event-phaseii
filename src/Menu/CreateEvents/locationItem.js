import React, { PureComponent } from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";

class LocationItem extends PureComponent {
  constructor(props) {
    super(props);

    this._handlePress = this._handlePress.bind(this);
  }

  _handlePress = async () => {
    const res = await this.props.fetchDetails(this.props.place_id);
    this.props.triggerParentElement(this.props.description, res);
    this.props.clearSearchResutls();
  };

  render() {
    return (
      <TouchableOpacity
        style={{
          paddingBottom: 10,
          paddingTop: 10,
          paddingLeft: 20,
          paddingRight: 20
        }}
        onPress={this._handlePress}
      >
        <Text>{this.props.description}</Text>
      </TouchableOpacity>
    );
  }
}

export default LocationItem;
