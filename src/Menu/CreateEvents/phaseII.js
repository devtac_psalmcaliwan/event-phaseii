import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Left,
  Body,
  Right,
  Icon,
  Picker,
  Label,
  Text,
  Button,
  Textarea,
  List,
  ActionSheet
} from "native-base";
import Spinner from "react-native-loading-spinner-overlay";
import ImagePicker from "react-native-image-crop-picker";
import { Observable } from "rxjs";
import { forkJoin } from "rxjs/observable/forkJoin";
import { Col, Row, Grid } from "react-native-easy-grid";
import { View, Image, ScrollView, TouchableOpacity } from "react-native";
import { Auth } from "./../../Library/Auth";
import firebase from "react-native-firebase";
import LocationItem from "./locationItem";
import DatePicker from "react-native-datepicker";
import { GoogleAutoComplete } from "react-native-google-autocomplete";
import { Generate } from "./../../Library/generate";

var BUTTONS = [
  "Pick Primary Image",
  "Pick Gallery Image",
  "Pick Gallery Video",
  "Delete",
  "Cancel"
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
export default class eventCreactionPhaseII extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      visible: false,
      image: {},
      primaryIMG: [],
      clicked: "",
      images: [],
      imgURI:
        "https://firebasestorage.googleapis.com/v0/b/event-management-66d93.appspot.com/o/assets%2Fif_user_1287507.png?alt=media&token=dcb2386a-69bd-43cb-805f-732776392745",
      user: {
        email: "",
        userType: "",
        name: {
          middleName: "",
          nickName: "",
          lastName: "",
          firstName: ""
        },
        mobile: "",
        password: "",
        InCaseOfEmergency: {
          ceFirstName: "",
          ceMobile: "",
          ceRelationship: "",
          ceEmail: "",
          ceLastName: ""
        },
        dateRegistered: "",
        uid: "",
        username: "",
        imgURL: ""
      },
      whereToPost: this.props.navigation.getParam("whereToPost"),
      primary: "",
      gallery: [],
      slides: [],

      title: "",
      description: "",
      tagline: "",
      venue: "",
      schedule: "",
      dateFrom: "",
      timeFrom: "",
      dateTo: "",
      timeTo: "",

      location: "",
      regionSet: {
        lat: "",
        lng: ""
      }
    };

    this.setPlaceQuery = this.setPlaceQuery.bind(this);
  }

  componentDidMount() {
    const images = this.props.navigation.getParam("images");
    const primary = this.props.navigation.getParam("primary");

    if (primary != null) {
      this.setState({ primary: primary });
    }
    if (images != null) {
      this.setState({ images: images });
    }

    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data(),
          imgURI: user.data().imgURL
        });
      });
  }

  uploadPrimary() {
    this.setState({
      visible: true
    });
    const primary = this.state.primary;

    let text = Generate.generateID();
    const object = {
      id: text
    };

    const event = "events";
    const pri = "primary";

    firebase
      .storage()
      .ref(`${event}/${object.id}/${pri}`)
      .child(object.id)
      .putFile(primary.path)
      .then(url => {
        let primaryURL = url.downloadURL;
        this.uploadGallery(primaryURL, object);
      });
  }

  uploadGallery(primaryURL, object) {
    let newArr = new Array();
    this.state.images.map(rec => {
      newArr.push(this.partialUpload(rec, object.id));
    });

    forkJoin(newArr).subscribe(url => {
      url.map(u => {
        let data = {
          documentID: Generate.generateID(),
          eventID: object.id,
          galleryURL: u.downloadURL
        };
        firebase
          .firestore()
          .collection("event-gallery")
          .doc(data.documentID)
          .set(data);
      });

      this.fillData(primaryURL, object);
    });
  }

  partialUpload(rec, id) {
    const event = "events";
    const gallery = "gallery";

    let text = Generate.generateID();
    let obg = {
      id: text
    };

    return Observable.create(obsr => {
      firebase
        .storage()
        .ref(`${event}/${id}/${gallery}`)
        .child(obg.id)
        .putFile(rec.path)
        .then(url => {
          obsr.next(url);
          obsr.complete();
        });
    });
  }

  fillData(primaryURL, object) {
    let data = {
      eventID: object.id,
      creator: this.userData.uid,
      creatorData: {
        name: {
          first: this.state.user.name.first,
          middle: this.state.user.name.middle,
          last: this.state.user.name.last
        },
        contact: {
          mobile: this.state.user.mobile,
          email: this.state.user.email
        }
      },
      createdOn: new Date(),
      whereToPost: this.state.whereToPost,
      title: this.state.title,
      description: this.state.description,
      venue: this.state.location,
      schedule: this.state.schedule,
      primaryURL: primaryURL,
      schedule: {
        dateFrom: this.state.dateFrom,
        timeFrom: this.state.timeFrom,
        dateTo: this.state.dateTo,
        timeTo: this.state.timeTo
      },
      regionSet: {
        lat: this.state.regionSet.lat,
        lng: this.state.regionSet.lng
      }
    };

    firebase
      .firestore()
      .collection("events")
      .doc(object.id)
      .set(data)
      .then(() => {
        this.setState({
          visible: false
        }),
          this.props.navigation.goBack();
      });
  }

  setPlaceQuery(description, res = false) {
    this.setState({
      queryPlace: description,
      location: description,
      regionSet: {
        lat: res.geometry.location.lat,
        lng: res.geometry.location.lng
      }
    });
  }

  imagePicker(clicked) {
    if (clicked == "Pick Primary Image") {
      ImagePicker.openPicker({
        width: 300,
        height: 400
      }).then(primaryIMG => {
        this.setState({
          primary: primaryIMG,
          active: true
        });
      });
    }

    if (clicked == "Pick Gallery Image") {
      ImagePicker.openPicker({
        multiple: true
      }).then(images => {
        let gallery = new Array();

        images.forEach(images => {
          gallery.push(images);
        });

        this.setState({
          active: true,
          images: gallery
        });
      });
    }
  }

  render() {
    return (
      <Container>
        <Header>
          <Left>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("MyEventsScreen");
              }}
            >
              <Icon
                type="MaterialIcons"
                name="chevron-left"
                style={{ color: "white" }}
              />
            </TouchableOpacity>
          </Left>
          <Body />
          <Right>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("MyFeeds");
              }}
            >
              <Icon
                type="MaterialIcons"
                name="clear"
                style={{ color: "white" }}
              />
            </TouchableOpacity>
          </Right>
        </Header>

        <Content>
          <Spinner
            visible={this.state.visible}
            textContent={"Creating Event ... Please wait"}
            textStyle={{ color: "#FFF" }}
          />
          <View
            style={{
              padding: 20
            }}
          >
            <Grid>
              <Col style={{ width: "20%" }}>
                <Image
                  source={{
                    uri:
                      this.state.user.imageURL == ""
                        ? this.state.imgURI
                        : this.state.user.imageURL
                  }}
                  style={{ width: 50, height: 50, borderRadius: 50 / 2 }}
                />
              </Col>
              <Col style={{ width: "80%" }}>
                <Text>
                  {this.state.user.name.first} {this.state.user.name.middle}{" "}
                  {this.state.user.name.last}
                </Text>
                <Text note>{this.state.whereToPost}</Text>
              </Col>
            </Grid>
          </View>

          <Form>
            <Form>
              <Item floatingLabel>
                <Label>TITLE</Label>
                <Input
                  value={this.state.title}
                  onChangeText={text => this.setState({ title: text })}
                />
              </Item>
              <Item floatingLabel>
                <Label>DESCRIPTION</Label>
                <Input
                  value={this.state.description}
                  onChangeText={text => this.setState({ description: text })}
                />
              </Item>

              <Item floatingLabel>
                <Label>TAGLINE</Label>
                <Input
                  value={this.state.tagline}
                  onChangeText={text => this.setState({ tagline: text })}
                />
              </Item>
              <View style={{ paddingLeft: 20, paddingTop: 20 }}>
                <Label>VENUE</Label>
              </View>
              <GoogleAutoComplete
                apiKey={"AIzaSyCEUwnhKcAvv9y7notB-VQWuXl4zQlct08"}
                debounce={200}
                minLength={2}
              >
                {({
                  handleTextChange,
                  locationResults,
                  fetchDetails,
                  isSearching,
                  inputValue,
                  clearSearchs
                }) => (
                  <React.Fragment>
                    <Input
                      onChangeText={handleTextChange}
                      value={
                        this.state.queryPlace != null
                          ? this.state.queryPlace
                          : null
                      }
                      placeholder={
                        "____________________________________________"
                      }
                    />

                    <ScrollView>
                      {locationResults.map(el => (
                        <LocationItem
                          triggerParentElement={this.setPlaceQuery}
                          clearSearchResutls={clearSearchs}
                          {...el}
                          key={el.id}
                          fetchDetails={fetchDetails}
                        />
                      ))}
                    </ScrollView>
                  </React.Fragment>
                )}
              </GoogleAutoComplete>

              <View style={{ padding: 20 }}>
                <React.Fragment>
                  <Text>From</Text>
                </React.Fragment>
                <Grid>
                  <Col>
                    <DatePicker
                      style={{ width: 150 }}
                      date={this.state.dateFrom}
                      mode="date"
                      placeholder="select date"
                      format="YYYY-MM-DD"
                      minDate={new Date()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={dateFrom => {
                        this.setState({ dateFrom: dateFrom });
                      }}
                    />
                  </Col>
                  <Col>
                    <DatePicker
                      style={{ width: 150 }}
                      date={this.state.timeFrom}
                      mode="time"
                      placeholder="select time"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={timeFrom => {
                        this.setState({ timeFrom: timeFrom });
                      }}
                    />
                  </Col>
                </Grid>
              </View>

              <View style={{ padding: 20 }}>
                <React.Fragment>
                  <Text>To</Text>
                </React.Fragment>
                <Grid>
                  <Col>
                    <DatePicker
                      style={{ width: 150 }}
                      date={this.state.dateTo}
                      mode="date"
                      placeholder="select date"
                      format="YYYY-MM-DD"
                      minDate={new Date()}
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={dateTo => {
                        this.setState({ dateTo: dateTo });
                      }}
                    />
                  </Col>
                  <Col>
                    <DatePicker
                      style={{ width: 150 }}
                      date={this.state.timeTo}
                      mode="time"
                      placeholder="select time"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      onDateChange={timeTo => {
                        this.setState({ timeTo: timeTo });
                      }}
                    />
                  </Col>
                </Grid>
              </View>

              <Grid>
                <Col>
                  <Image
                    source={{
                      uri: this.state.primary.path
                    }}
                    style={{ width: 50, height: 50 }}
                  />
                </Col>
                {this.state.images.map(i => (
                  <Col>
                    <Image
                      source={{
                        uri: i.path
                      }}
                      style={{ width: 50, height: 50 }}
                    />
                  </Col>
                ))}
              </Grid>

              <View style={{ padding: 20 }}>
                <Button
                  primary
                  full
                  onPress={() =>
                    ActionSheet.show(
                      {
                        options: BUTTONS,
                        cancelButtonIndex: CANCEL_INDEX,
                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                        title: "Media Uploader"
                      },
                      buttonIndex => {
                        this.setState({ clicked: BUTTONS[buttonIndex] }, () => {
                          this.imagePicker(this.state.clicked);
                        });
                      }
                    )
                  }
                >
                  <Text>PHOTO/VIDEO</Text>
                </Button>
              </View>
            </Form>
          </Form>

          <Grid>
            <Col
              style={{
                width: "100%",
                paddingLeft: 20,
                paddingRight: 20
              }}
            >
              <Button
                success
                full
                onPress={() => {
                  this.uploadPrimary();
                }}
              >
                <Text>Create</Text>
              </Button>
            </Col>
          </Grid>
        </Content>
      </Container>
    );
  }
}
