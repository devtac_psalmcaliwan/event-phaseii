import React, { Component } from "react";
import {
  Container,
  Header,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text,
  Icon,
  Left,
  Body,
  Right,
  List,
  ListItem,
  Button,
  Switch,
  Separator
} from "native-base";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Col, Row, Grid } from "react-native-easy-grid";
import NavigationIndex from "./../NavigationBar/index";
import Spinner from "react-native-loading-spinner-overlay";
import { Auth } from "./../Library/Auth";
import firebase from "react-native-firebase";
export default class MenuIndex extends Component {
  userData = Auth.getUser();
  constructor(props) {
    super(props);

    this.state = {
      user: {
        dateRegistered: "",
        email: "",
        flag: "",
        imageURL: "",
        mobile: "",
        name: {
          first: "",
          last: "",
          middle: ""
        },
        userID: "",
        userType: ""
      },
      visible: false,
      groups: []
    };
  }
  componentDidMount() {
    firebase
      .firestore()
      .collection("users")
      .doc(this.userData.uid)
      .get()
      .then(user => {
        this.setState({
          user: user.data()
        });
      });

    firebase
      .firestore()
      .collection("Groups")
      .onSnapshot(doc => {
        let groups = new Array();

        doc.forEach(doc => {
          groups.push(doc.data());

          this.setState({
            groups: groups
          });
        });
      });
  }

  logout() {
    this.setState({ visible: false });
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.props.navigation.navigate("LoginIndex");
      });
  }
  render() {
    return (
      <Container>
        {console.log(this.state.groups)}
        <NavigationIndex menu={"white"} />
        <Content>
          <Spinner
            visible={this.state.visible}
            textContent={"Going to logout.... Please wait"}
            textStyle={{ color: "#FFF" }}
          />
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              padding: 10,
              backgroundColor: "gray"
            }}
          >
            <Image
              source={{ uri: this.state.user.imageURL }}
              style={{
                height: 150,
                width: 150,
                borderRadius: 150
              }}
            />
          </View>

          <Separator bordered>
            <Text>Menu</Text>
          </Separator>
          <ListItem icon>
            <Left>
              <Button transparent>
                <Icon active name="event" type="MaterialIcons" />
              </Button>
            </Left>
            <Body>
              <Text>Events Management</Text>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("MyFeeds");
                }}
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>

          <Separator>
            <Text>Groups</Text>
          </Separator>
          {this.state.groups.map(g => (
            <ListItem>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("GroupOpen", {
                    params: g.groupID
                  });
                }}
              >
                <Text>{g.groupName}</Text>
              </TouchableOpacity>
            </ListItem>
          ))}

          <ListItem>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("GroupIndex");
              }}
            >
              <Text>more</Text>
            </TouchableOpacity>
          </ListItem>
        </Content>

        <ListItem icon>
          <Left>
            <Button transparent>
              <Icon active name="portrait" type="MaterialIcons" />
            </Button>
          </Left>
          <Body>
            <Text>My Profile</Text>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("ProfileIndex");
              }}
            >
              <Icon active name="arrow-forward" />
            </TouchableOpacity>
          </Right>
        </ListItem>

        <ListItem icon>
          <Left>
            <Button transparent>
              <Icon name="more-horiz" type="MaterialIcons" />
            </Button>
          </Left>
          <Body>
            <Text>Change Password</Text>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("ChangePassword");
              }}
            >
              <Icon active name="arrow-forward" />
            </TouchableOpacity>
          </Right>
        </ListItem>
        <ListItem icon>
          <Left>
            <Button transparent>
              <Icon name="power-settings-new" type="MaterialIcons" />
            </Button>
          </Left>
          <Body>
            <Text>Logout</Text>
          </Body>
          <Right>
            <TouchableOpacity
              onPress={() => {
                this.setState({ visible: true });
                this.logout();
              }}
            >
              <Icon name="arrow-forward" />
            </TouchableOpacity>
          </Right>
        </ListItem>
      </Container>
    );
  }
}
